<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login']]);
        $this->middleware('jwt', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function SignUp(Request $request){

//        $adminRole = Role::where('name',$request['role_users'])->first();
        $newUser = \App\User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'allhospital' => "",
        ]);
        $newUser->attachRole("4");

        return response()->json([
            'response_message' => "success",
            'response_status' =>200
        ]);
    }
    public function UserInfo(Request $request){
        $id = $request['id'];
        $userinfo = User::where('id',$id)->value('allhospital');

        if($userinfo == Null){
            $user = User::where('id',$id)->get();
            foreach ($user as $data){
                $this->RoleUse($data);
                $data['role'] = $this->RoleUse($data);
            }
            return $user->toArray();

        }else{
            $user = User::where('users.id',$id)
                ->join('HospitalInfo', 'HospitalInfo.id', '=', 'users.allhospital')
                ->select('HospitalInfo.facility_name', 'users.*')->get();
            foreach ($user as $data){
                $this->RoleUse($data);
                $data['role'] = $this->RoleUse($data);
            }
            return $user->toArray();
        }

    }
    public function RoleUse($data){
        $role = DB::table('role_user')->where('user_id',$data->id)->value('role_id');
        $rolename = Role::where('id',$role)->value('name');
        return $rolename;
    }
    public function DeleteAccount(Request $request){
        $id = $request['id'];
        $delete = User::where('id', $id)->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'You have successfully deleted account', 'response_status' => true);
        }
        return Response()->json($arr);
    }
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                'error' => 'Unauthorized',
                'response_message' => "Email or password is not matching",
                'response_status' =>200
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $token = JWTAuth::fromUser($user,[
            'email' =>$user->email,
            'role' =>isset($user->roles[0]) ? $user->roles[0]->display_name:NULL
        ]);
        $role = isset($user->roles[0]) ? $user->roles[0]->display_name:NULL;
        return response()->json(compact('role','user'));
//        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */

//    public function logout(){
//        $user = JWTAuth::parseToken()->authenticate();
//
//        $user->login_status = "no";
//        $user->save();
////        return $user;
////    Auth::logout();
//        JWTAuth::invalidate(JWTAuth::getToken());
//        return json_encode("logged out");
//    }
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

}
