-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 17, 2020 at 11:09 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `primate`
--

-- --------------------------------------------------------

--
-- Table structure for table `agents`
--

CREATE TABLE `agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agents`
--

INSERT INTO `agents` (`id`, `agent_number`, `agent_name`, `agent_email`, `agent_phone`, `agent_country`, `agent_city`, `agent_state`, `agent_address`, `agent_notes`, `created_at`, `updated_at`) VALUES
(1, 'AG-690', 'Elysee Travels', 'ccelyse1@gmail.com', '0782384772', '184', 'kigali', 'kigali', 'kabeza', 'abroad', '2020-02-06 05:09:47', '2020-02-06 05:09:47'),
(2, 'AG-554', 'Safari Travels', 'kalisa913@gmail.com', '0782384772', '184', 'kigali', 'kigali', 'kk 228st', 'Travels', '2020-02-06 05:16:59', '2020-02-06 05:16:59');

-- --------------------------------------------------------

--
-- Table structure for table `apps_countries`
--

CREATE TABLE `apps_countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apps_countries`
--

INSERT INTO `apps_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CD', 'Democratic Republic of the Congo'),
(50, 'CG', 'Republic of Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DK', 'Denmark'),
(58, 'DJ', 'Djibouti'),
(59, 'DM', 'Dominica'),
(60, 'DO', 'Dominican Republic'),
(61, 'TP', 'East Timor'),
(62, 'EC', 'Ecuador'),
(63, 'EG', 'Egypt'),
(64, 'SV', 'El Salvador'),
(65, 'GQ', 'Equatorial Guinea'),
(66, 'ER', 'Eritrea'),
(67, 'EE', 'Estonia'),
(68, 'ET', 'Ethiopia'),
(69, 'FK', 'Falkland Islands (Malvinas)'),
(70, 'FO', 'Faroe Islands'),
(71, 'FJ', 'Fiji'),
(72, 'FI', 'Finland'),
(73, 'FR', 'France'),
(74, 'FX', 'France, Metropolitan'),
(75, 'GF', 'French Guiana'),
(76, 'PF', 'French Polynesia'),
(77, 'TF', 'French Southern Territories'),
(78, 'GA', 'Gabon'),
(79, 'GM', 'Gambia'),
(80, 'GE', 'Georgia'),
(81, 'DE', 'Germany'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GK', 'Guernsey'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'IM', 'Isle of Man'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'CI', 'Ivory Coast'),
(109, 'JE', 'Jersey'),
(110, 'JM', 'Jamaica'),
(111, 'JP', 'Japan'),
(112, 'JO', 'Jordan'),
(113, 'KZ', 'Kazakhstan'),
(114, 'KE', 'Kenya'),
(115, 'KI', 'Kiribati'),
(116, 'KP', 'Korea, Democratic People\'s Republic of'),
(117, 'KR', 'Korea, Republic of'),
(118, 'XK', 'Kosovo'),
(119, 'KW', 'Kuwait'),
(120, 'KG', 'Kyrgyzstan'),
(121, 'LA', 'Lao People\'s Democratic Republic'),
(122, 'LV', 'Latvia'),
(123, 'LB', 'Lebanon'),
(124, 'LS', 'Lesotho'),
(125, 'LR', 'Liberia'),
(126, 'LY', 'Libyan Arab Jamahiriya'),
(127, 'LI', 'Liechtenstein'),
(128, 'LT', 'Lithuania'),
(129, 'LU', 'Luxembourg'),
(130, 'MO', 'Macau'),
(131, 'MK', 'North Macedonia'),
(132, 'MG', 'Madagascar'),
(133, 'MW', 'Malawi'),
(134, 'MY', 'Malaysia'),
(135, 'MV', 'Maldives'),
(136, 'ML', 'Mali'),
(137, 'MT', 'Malta'),
(138, 'MH', 'Marshall Islands'),
(139, 'MQ', 'Martinique'),
(140, 'MR', 'Mauritania'),
(141, 'MU', 'Mauritius'),
(142, 'TY', 'Mayotte'),
(143, 'MX', 'Mexico'),
(144, 'FM', 'Micronesia, Federated States of'),
(145, 'MD', 'Moldova, Republic of'),
(146, 'MC', 'Monaco'),
(147, 'MN', 'Mongolia'),
(148, 'ME', 'Montenegro'),
(149, 'MS', 'Montserrat'),
(150, 'MA', 'Morocco'),
(151, 'MZ', 'Mozambique'),
(152, 'MM', 'Myanmar'),
(153, 'NA', 'Namibia'),
(154, 'NR', 'Nauru'),
(155, 'NP', 'Nepal'),
(156, 'NL', 'Netherlands'),
(157, 'AN', 'Netherlands Antilles'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Reunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'KN', 'Saint Kitts and Nevis'),
(186, 'LC', 'Saint Lucia'),
(187, 'VC', 'Saint Vincent and the Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'SB', 'Solomon Islands'),
(200, 'SO', 'Somalia'),
(201, 'ZA', 'South Africa'),
(202, 'GS', 'South Georgia South Sandwich Islands'),
(203, 'SS', 'South Sudan'),
(204, 'ES', 'Spain'),
(205, 'LK', 'Sri Lanka'),
(206, 'SH', 'St. Helena'),
(207, 'PM', 'St. Pierre and Miquelon'),
(208, 'SD', 'Sudan'),
(209, 'SR', 'Suriname'),
(210, 'SJ', 'Svalbard and Jan Mayen Islands'),
(211, 'SZ', 'Swaziland'),
(212, 'SE', 'Sweden'),
(213, 'CH', 'Switzerland'),
(214, 'SY', 'Syrian Arab Republic'),
(215, 'TW', 'Taiwan'),
(216, 'TJ', 'Tajikistan'),
(217, 'TZ', 'Tanzania, United Republic of'),
(218, 'TH', 'Thailand'),
(219, 'TG', 'Togo'),
(220, 'TK', 'Tokelau'),
(221, 'TO', 'Tonga'),
(222, 'TT', 'Trinidad and Tobago'),
(223, 'TN', 'Tunisia'),
(224, 'TR', 'Turkey'),
(225, 'TM', 'Turkmenistan'),
(226, 'TC', 'Turks and Caicos Islands'),
(227, 'TV', 'Tuvalu'),
(228, 'UG', 'Uganda'),
(229, 'UA', 'Ukraine'),
(230, 'AE', 'United Arab Emirates'),
(231, 'GB', 'United Kingdom'),
(232, 'US', 'United States'),
(233, 'UM', 'United States minor outlying islands'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VU', 'Vanuatu'),
(237, 'VA', 'Vatican City State'),
(238, 'VE', 'Venezuela'),
(239, 'VN', 'Vietnam'),
(240, 'VG', 'Virgin Islands (British)'),
(241, 'VI', 'Virgin Islands (U.S.)'),
(242, 'WF', 'Wallis and Futuna Islands'),
(243, 'EH', 'Western Sahara'),
(244, 'YE', 'Yemen'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_number`, `client_name`, `client_email`, `client_phone`, `client_website`, `client_country`, `client_city`, `client_state`, `client_address`, `client_notes`, `created_at`, `updated_at`) VALUES
(1, 'CLI-327', 'Elysee bima', 'support@rwandahost.rw', '0782384772', 'https://registry.ricta.org.rw/', '1', 'kigali', 'kigali', 'kabeza', 'note', '2020-01-22 20:07:48', '2020-01-22 20:45:58'),
(2, 'CLI-203', 'Muhiren Twinky', 'ccelyse1@gmail.com', '0782384772', 'https://registry.ricta.org.rw/', '184', 'kigali', 'kigali', 'kabeza', 'note', '2020-01-22 20:47:23', '2020-01-22 20:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_10_202336_laratrust_setup_tables', 2),
(31, '2020_01_22_202124_create_clients_table', 22),
(34, '2020_02_01_072411_create_product_categories_table', 25),
(37, '2020_02_01_072741_create_product_providers_table', 26),
(38, '2020_01_23_203515_create_agents_table', 27),
(46, '2020_02_06_093351_create_provider_contracts_table', 29),
(47, '2020_01_22_230442_create_products_table', 30),
(48, '2020_02_06_072421_create_products_s_t_os_table', 31),
(51, '2020_02_06_133233_create_system_quotations_table', 33),
(53, '2020_02_12_082618_create_system_quotation_days_table', 34),
(54, '2020_02_17_211005_create_system_rooms_table', 35),
(55, '2020_02_17_211236_create_system_rooms_galleries_table', 35),
(56, '2020_02_06_133518_create_system_quote_details_table', 36),
(57, '2020_03_09_185940_create_categories_table', 37);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-users', 'Create Users', 'Create Users', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(2, 'read-users', 'Read Users', 'Read Users', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(3, 'update-users', 'Update Users', 'Update Users', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(4, 'delete-users', 'Delete Users', 'Delete Users', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(5, 'create-acl', 'Create Acl', 'Create Acl', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(6, 'read-acl', 'Read Acl', 'Read Acl', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(7, 'update-acl', 'Update Acl', 'Update Acl', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(8, 'delete-acl', 'Delete Acl', 'Delete Acl', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(9, 'read-profile', 'Read Profile', 'Read Profile', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(10, 'update-profile', 'Update Profile', 'Update Profile', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(11, 'create-profile', 'Create Profile', 'Create Profile', '2019-09-10 18:55:36', '2019-09-10 18:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(9, 2),
(9, 3),
(9, 4),
(9, 5),
(9, 6),
(9, 7),
(9, 8),
(10, 1),
(10, 2),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(10, 7),
(10, 8);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_user`
--

INSERT INTO `permission_user` (`permission_id`, `user_id`, `user_type`) VALUES
(9, 9, 'App\\User'),
(10, 9, 'App\\User'),
(11, 9, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_contract_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_category_id`, `provider_id`, `provider_contract_id`, `product_price`, `created_at`, `updated_at`) VALUES
(7, '1', '1', '2', '420', '2020-02-06 09:00:32', '2020-02-06 09:00:32'),
(8, '1', '1', '3', '450', '2020-02-06 09:01:47', '2020-02-06 09:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `products_s_t_os`
--

CREATE TABLE `products_s_t_os` (
  `id` int(10) UNSIGNED NOT NULL,
  `products_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_agent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_contract_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `products_sto_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_s_t_os`
--

INSERT INTO `products_s_t_os` (`id`, `products_id`, `products_agent_id`, `provider_contract_id`, `products_sto_price`, `created_at`, `updated_at`) VALUES
(3, '7', '1', '2', '60', '2020-02-06 09:00:32', '2020-02-06 09:00:32'),
(4, '7', '2', '2', '20', '2020-02-06 09:00:32', '2020-02-06 09:00:32'),
(5, '8', '1', '3', '100', '2020-02-06 09:01:47', '2020-02-06 09:01:47'),
(6, '8', '2', '3', '50', '2020-02-06 09:01:47', '2020-02-06 09:01:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_categories_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `product_categories_name`, `created_at`, `updated_at`) VALUES
(1, 'Accommodations', '2020-02-03 11:21:08', '2020-02-03 11:30:55'),
(5, 'Excursion', '2020-02-04 07:31:33', '2020-02-04 07:31:33'),
(6, 'Airport Transfer', '2020-02-04 07:31:46', '2020-02-04 07:31:46'),
(7, 'Transport', '2020-02-06 14:32:24', '2020-02-06 14:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `product_providers`
--

CREATE TABLE `product_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_providers`
--

INSERT INTO `product_providers` (`id`, `provider_name`, `product_category_id`, `created_at`, `updated_at`) VALUES
(1, 'Hotel Serena Kigali', '1', '2020-02-06 04:36:12', '2020-02-06 04:36:12'),
(2, 'Designated Driver', '6', '2020-02-06 05:03:19', '2020-02-06 05:03:19'),
(3, 'One and Only Nyungwe House', '1', '2020-02-06 13:47:27', '2020-02-06 13:47:27'),
(4, 'Elysee Safaris', '7', '2020-02-06 14:32:44', '2020-02-06 14:32:44');

-- --------------------------------------------------------

--
-- Table structure for table `provider_contracts`
--

CREATE TABLE `provider_contracts` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_contract_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `provider_contracts`
--

INSERT INTO `provider_contracts` (`id`, `product_category_id`, `provider_id`, `provider_contract_name`, `created_at`, `updated_at`) VALUES
(2, '1', '1', 'Standard', '2020-02-06 08:07:47', '2020-02-06 08:07:47'),
(3, '1', '1', 'Family room', '2020-02-06 08:08:17', '2020-02-06 08:08:17'),
(4, '1', '1', 'Suite room', '2020-02-06 08:08:30', '2020-02-06 08:08:30'),
(5, '1', '3', 'Cottages', '2020-02-06 13:48:39', '2020-02-06 13:48:39'),
(6, '7', '4', 'Vehicle per day', '2020-02-06 14:33:04', '2020-02-06 14:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadministrator', 'Superadministrator', 'Superadministrator', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(2, 'administrator', 'Administrator', 'Administrator', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(3, 'user', 'User', 'User', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(4, 'admin', 'Admin', 'Admin', '2019-09-10 18:55:35', '2019-09-10 18:55:35'),
(5, 'hospital', 'Hospital', 'Hospital', '2019-09-10 18:55:36', '2019-09-10 18:55:36'),
(6, 'surveyors', 'Surveyors', 'Surveyors', '2019-09-10 18:55:36', '2019-09-10 18:55:36'),
(7, 'health_center', 'Health Center', 'Health Center', '2019-09-10 18:55:36', '2019-09-10 18:55:36'),
(8, 'moh', 'Moh', 'Moh', '2019-09-10 18:55:36', '2019-09-10 18:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'App\\User'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\User'),
(2, 2, 'App\\User'),
(3, 3, 'App\\User'),
(4, 4, 'App\\User'),
(5, 5, 'App\\User'),
(6, 6, 'App\\User'),
(7, 7, 'App\\User'),
(8, 8, 'App\\User'),
(4, 18, 'App\\User'),
(4, 19, 'App\\User'),
(4, 20, 'App\\User'),
(5, 21, 'App\\User');

-- --------------------------------------------------------

--
-- Table structure for table `system_quotations`
--

CREATE TABLE `system_quotations` (
  `id` int(10) UNSIGNED NOT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quotation_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `travel_start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_quotations`
--

INSERT INTO `system_quotations` (`id`, `agent_id`, `quotation_name`, `travel_start_date`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '1', 'Stunner', '2020-02-12', '4', '2020-02-11 04:09:01', '2020-02-11 04:09:01'),
(4, '2', 'King Munya', '2020-03-18', '4', '2020-03-17 19:02:04', '2020-03-17 19:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `system_quotation_days`
--

CREATE TABLE `system_quotation_days` (
  `id` int(10) UNSIGNED NOT NULL,
  `quotation_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quotation_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_quotation_days`
--

INSERT INTO `system_quotation_days` (`id`, `quotation_id`, `agent_id`, `quotation_day`, `created_at`, `updated_at`) VALUES
(13, '1', '1', '2020-02-12', '2020-02-25 07:12:18', '2020-02-25 07:12:18'),
(14, '1', '1', '2020-02-13', '2020-02-25 07:12:44', '2020-02-25 07:12:44'),
(15, '4', '2', '2020-03-18', '2020-03-17 19:20:35', '2020-03-17 19:20:35'),
(17, '4', '2', '2020-03-19', '2020-03-17 19:24:57', '2020-03-17 19:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `system_quote_details`
--

CREATE TABLE `system_quote_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `system_quotation_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_quotation_day_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_contract_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rack_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_cost` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_rack_rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_quote_details`
--

INSERT INTO `system_quote_details` (`id`, `system_quotation_id`, `system_quotation_day_id`, `product_category_id`, `provider_id`, `provider_contract_id`, `quantity`, `rack_rate`, `unit_cost`, `total_price`, `total_rack_rate`, `created_at`, `updated_at`) VALUES
(3, '1', '13', '1', '1', '2', '2', '420', '60', '120', '840', '2020-02-25 07:12:18', '2020-02-25 07:12:18'),
(4, '1', '13', '1', '1', '3', '3', '450', '100', '300', '1350', '2020-02-25 07:12:18', '2020-02-25 07:12:18'),
(5, '1', '14', '1', '1', '3', '4', '450', '100', '400', '1800', '2020-02-25 07:12:44', '2020-02-25 07:12:44'),
(6, '1', '14', '1', '1', '2', '6', '420', '60', '360', '2520', '2020-02-25 07:12:44', '2020-02-25 07:12:44'),
(7, '4', '15', '1', '1', '2', '2', '420', '20', '40', '840', '2020-03-17 19:20:35', '2020-03-17 19:20:35'),
(8, '4', '15', '1', '1', '3', '2', '450', '50', '100', '900', '2020-03-17 19:20:35', '2020-03-17 19:20:35'),
(10, '4', '17', '1', '1', '2', '3', '420', '20', '60', '1260', '2020-03-17 19:24:57', '2020-03-17 19:24:57');

-- --------------------------------------------------------

--
-- Table structure for table `system_rooms`
--

CREATE TABLE `system_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_bed_choice` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_addons` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_rooms_galleries`
--

CREATE TABLE `system_rooms_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_picture_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allhospital` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `allhospital`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Steve Ishimwe', 'admin@app.com', '$2y$12$m6/o/jzkXDmLLo6kNMo3yOU2hv5b36FxD5E18RaM25zZ6ftxYPTOW', NULL, NULL, '2019-09-10 18:55:36', '2019-09-10 18:55:36'),
(15, 'Joel Muhizi', 'joel@app.com', '$2y$10$fdnjIOdRuMObIGfjKuaayemNauB4roTiXGGykt1NqejNLt7Cdfc/u', '5', NULL, '2019-11-20 10:45:55', '2019-11-20 10:45:55'),
(18, 'stunner', 'stunner@admin.com', '$2y$10$uQe5YJlCsk6PGqcXg/CoGOflDjJ3q4jTvw65LXx0VJV0v7ecHHBla', '', NULL, '2020-01-22 17:52:29', '2020-01-22 17:52:29'),
(19, 'Joe', 'joe@app.com', '$2y$10$eDR4b7BZIjdk3Gjm635IKugLZMRCes3/KgExD05q6Aue/yOecGikK', '', NULL, '2020-01-22 17:55:46', '2020-01-22 17:55:46'),
(20, 'Muhiren Twinky', 'muhire@app.com', '$2y$10$o1B9LYUDpHxVH8ddJ9oxeeG6I8kFKmC10joELBBXyPY.tJGewuA5u', '', NULL, '2020-01-22 17:56:54', '2020-01-22 17:56:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apps_countries`
--
ALTER TABLE `apps_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_s_t_os`
--
ALTER TABLE `products_s_t_os`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_providers`
--
ALTER TABLE `product_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider_contracts`
--
ALTER TABLE `provider_contracts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `system_quotations`
--
ALTER TABLE `system_quotations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_quotation_days`
--
ALTER TABLE `system_quotation_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_quote_details`
--
ALTER TABLE `system_quote_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_rooms`
--
ALTER TABLE `system_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_rooms_galleries`
--
ALTER TABLE `system_rooms_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apps_countries`
--
ALTER TABLE `apps_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products_s_t_os`
--
ALTER TABLE `products_s_t_os`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_providers`
--
ALTER TABLE `product_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `provider_contracts`
--
ALTER TABLE `provider_contracts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `system_quotations`
--
ALTER TABLE `system_quotations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `system_quotation_days`
--
ALTER TABLE `system_quotation_days`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `system_quote_details`
--
ALTER TABLE `system_quote_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `system_rooms`
--
ALTER TABLE `system_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_rooms_galleries`
--
ALTER TABLE `system_rooms_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
