var data  = sessionStorage.getItem('accessToken');
// alert(data);
//     if(data == null){
//         window.location.href = "/";
//     }
    if(sessionStorage.getItem("accessToken") === null){
            window.location.href = "/";
        }
    else{

         $("#apptopmenu").html("<button id='sidebarToggleTop' class='btn btn-link d-md-none rounded-circle mr-3'> <i class='fa fa-bars'></i> </button> <ul class='navbar-nav ml-auto' style='display: none'> <li class='nav-item dropdown no-arrow d-sm-none'> <a class='nav-link dropdown-toggle' href='#' id='searchDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-search fa-fw'></i> </a> <div class='dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in' aria-labelledby='searchDropdown'> <form class='form-inline mr-auto w-100 navbar-search'> <div class='input-group'> <input type='text' class='form-control bg-light border-0 small' placeholder='Search for...' aria-label='Search' aria-describedby='basic-addon2'> <div class='input-group-append'> <button class='btn btn-primary' type='button'> <i class='fas fa-search fa-sm'></i> </button> </div></div></form> </div></li><li class='nav-item dropdown no-arrow mx-1'> <a class='nav-link dropdown-toggle' href='#' id='alertsDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-bell fa-fw'></i> <span class='badge badge-danger badge-counter'>3+</span> </a> <div class='dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in' aria-labelledby='alertsDropdown'> <h6 class='dropdown-header'> Alerts Center </h6> <a class='dropdown-item d-flex align-items-center' href='#'> <div class='mr-3'> <div class='icon-circle bg-primary'> <i class='fas fa-file-alt text-white'></i> </div></div><div> <div class='small text-gray-500'>December 12, 2019</div><span class='font-weight-bold'>A new monthly report is ready to download!</span> </div></a> <a class='dropdown-item text-center small text-gray-500' href='#'>Show All Alerts</a> </div></li><li class='nav-item dropdown no-arrow mx-1'> <a class='nav-link dropdown-toggle' href='#' id='messagesDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <i class='fas fa-envelope fa-fw'></i> <span class='badge badge-danger badge-counter'>7</span> </a> <div class='dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in' aria-labelledby='messagesDropdown'> <h6 class='dropdown-header'> Message Center </h6> <a class='dropdown-item d-flex align-items-center' href='#'> <div class='dropdown-list-image mr-3'> <img class='rounded-circle' src='https://source.unsplash.com/fn_BT9fwg_E/60x60' alt=''> <div class='status-indicator bg-success'></div></div><div class='font-weight-bold'> <div class='text-truncate'>Hi there! I am wondering if you can help me with a problem I've been having.</div><div class='small text-gray-500'>Emily Fowler · 58m</div></div></a> </div></li><div class='topbar-divider d-none d-sm-block'></div><li class='nav-item dropdown no-arrow'> <a class='nav-link dropdown-toggle' href='#' id='userDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> <span class='mr-2 d-none d-lg-inline text-gray-600 small'><div id='appusername' style='margin-top: 20px;'></div></span> <img class='img-profile rounded-circle' src='../backend/img/ingenuitlogo-300x149.png'> </a> <div class='dropdown-menu dropdown-menu-right shadow animated--grow-in' aria-labelledby='userDropdown'> <a class='dropdown-item' href='#'> <i class='fas fa-user fa-sm fa-fw mr-2 text-gray-400'></i> Profile </a> <a class='dropdown-item' href='#'> <i class='fas fa-cogs fa-sm fa-fw mr-2 text-gray-400'></i> Settings </a> <div class='dropdown-divider'></div><a class='dropdown-item' href='#' data-toggle='modal' data-target='#logoutModal'> <i class='fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400'></i> Logout </a> </div></li></ul>");

        $(document).ready(function () {
            $(".note-popover").remove();
            $.ajax({
                type: 'post',
                url: "../api/auth/me",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {
                    // console.log();

                    if (response.user.roles[0].name == 'admin'){
                        $("#sidebarmenu").html("<ul class='navbar-nav bg-gradient-primary sidebar sidebar-dark accordion' id='accordionSidebar'><a class='sidebar-brand d-flex align-items-center justify-content-center' href='/SystemQuotation'> <div class='sidebar-brand-icon rotate-n-15'> <i class='fas fa-laugh-wink'></i> </div><div class='sidebar-brand-text mx-3'>Primate Safaris <sup>1</sup></div></a> <div class='mx-auto'><img data-name='"+response.user.name+"' class='img-profile'/></div><p class='text-center'><span id='appusername'></span></p><p class='m-0 text-center'><i class='fa fa-power-off' id='logout-link'></i></p><div class='row bellow-profile'><div class='col-6 p-0'><div class='admin-grid-col-1'><span id='countusers'></span><p>System Users</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-2'><span id='countagent'></span><p>Clients</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-3'><span id='pendingQuotation'></span><p>Pending Quotations</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-4'><span id='approvedQuotation'></span><p>Approved Quotations</p></div></div></div></div></div><li class='nav-item'> <a class='nav-link' href='/SystemQuotation'> <i class='flaticon flaticon-diagram'></i> <span>Dashboard</span></a> </li> <div class='sidebar-heading'> System Settings </div><li class='nav-item'> <a class='nav-link' href='/CreateAccount'><i class='flaticon flaticon-hospital-3'></i> <span>System Users</span></a> </li><li class='nav-item'> <a class='nav-link' href='/SystemAgents'><i class='flaticon flaticon-user'></i> <span>Clients</span></a> </li><div class='text-center d-none d-md-inline'><div class='sidebar-heading'> Products </div><li class=\"nav-item\"> <a class=\"nav-link collapsed\" href='#' data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"true\" aria-controls=\"collapseTwo\"> <i class=\"flaticon flaticon-checklist\"></i> <span>Products</span> </a> <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordionSidebar\"> <div class=\"bg-white py-2 collapse-inner rounded\"> <a class=\"collapse-item\" href='/SystemProductCategories'>Product services</a><a class=\"collapse-item\" href='/SystemProductProvider'>Provider name</a><a class=\"collapse-item\" href='/SystemContracts'>Provider Contracts</a><a class=\"collapse-item\" href='/SystemProducts'>Products</a></div></div></li><li class='nav-item'> <a class='nav-link' href='/SystemQuotation'><i class='flaticon flaticon-hospital-2'></i> <span>Quotations</span></a> </li></div></ul>");

                    }
                    else if(response.user.roles[0].name == 'hospital'){
                        $("#sidebarmenu").html("<ul class='navbar-nav bg-gradient-primary sidebar sidebar-dark accordion' id='accordionSidebar'><a class='sidebar-brand d-flex align-items-center justify-content-center' href='/HDashboard'> <div class='sidebar-brand-icon rotate-n-15'> <i class='fas fa-laugh-wink'></i> </div><div class='sidebar-brand-text mx-3'>Accreditx <sup>1</sup></div></a> <div class='mx-auto'><img data-name='"+response.user.name+"' class='img-profile'/></div><p class='text-center'><span id='appusername'></span></p><p class='m-0 text-center'><i class='fa fa-power-off' id='logout-link'></i></p><div class='row bellow-profile'><div class='col-6 p-0'><div class='admin-grid-col-1'><span id='countusers'></span><p>Provision Hospital</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-2'><span id='countagent'></span><p>District Hospital</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-3'><span id='pendingQuotation'></span><p>Health Centers</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-4'><span id='approvedQuotation'></span><p>Risk Area</p></div></div></div></div></div><li class='nav-item'> <a class='nav-link' href='/HDashboard'> <i class='flaticon flaticon-diagram'></i> <span>Dashboard</span></a> </li> <div class='sidebar-heading'> System Settings </div><li class='nav-item'> <a class='nav-link' href='SystemAccount'><i class='fas fa-fw fa-cog'></i> <span>System user</span></a> </li><li class='nav-item'> <a class='nav-link' href='HospitalProfile'><i class='fas fa-fw fa-cog'></i> <span>Hospital profile</span></a> </li><li class='nav-item'> <a class='nav-link' href='/ShowHospitalS'><i class='flaticon flaticon-hospital'></i> <span>Hospital/Health center information</span></a> </li> <div class='text-center d-none d-md-inline'> </div></ul>");
                    }else if(response.user.roles[0].name == 'surveyors'){
                        $("#sidebarmenu").html("<ul class='navbar-nav bg-gradient-primary sidebar sidebar-dark accordion' id='accordionSidebar'><a class='sidebar-brand d-flex align-items-center justify-content-center' href='/dashboard'> <div class='sidebar-brand-icon rotate-n-15'> <i class='fas fa-laugh-wink'></i> </div><div class='sidebar-brand-text mx-3'>Accreditx <sup>1</sup></div></a> <div class='mx-auto'><img data-name='"+response.user.name+"' class='img-profile'/></div><p class='text-center'><span id='appusername'></span></p><p class='m-0 text-center'><i class='fa fa-power-off' id='logout-link'></i></p><div class='row bellow-profile'><div class='col-6 p-0'><div class='admin-grid-col-1'><span id='countusers'></span><p>Provision Hospital</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-2'><span id='countagent'></span><p>District Hospital</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-3'><span id='pendingQuotation'></span><p>Health Centers</p></div></div><div class='col-6 p-0'><div class='admin-grid-col-4'><span id='approvedQuotation'></span><p>Risk Area</p></div></div></div></div></div><li class='nav-item'> <a class='nav-link' href='/dashboard'> <i class='flaticon flaticon-diagram'></i> <span>Dashboard</span></a> </li> <div class='sidebar-heading'> System Settings </div><li class='nav-item'> <a class='nav-link' href='SystemAccount'><i class='fas fa-fw fa-cog'></i> <span>Account profile</span></a> </li><li class='nav-item'> <a class='nav-link' href='SystemSurveyors'><i class='fas fa-fw fa-hospital'></i> <span>Surveyor Profile</span></a> </li><li class='nav-item'> <a class='nav-link' href='/SurveyHospital'><i class='flaticon flaticon-hospital'></i> <span>Hospital</span></a> </li><li class='nav-item'> <a class='nav-link' href='/ShowHealthCenterS'><i class='flaticon flaticon-hospital-2'></i> <span>Health center</span></a> </li><li class='nav-item'> <a class='nav-link' href='/SurveyReports'><i class='flaticon flaticon-hospital-2'></i> <span>Survey reports</span></a> </li><div class='sidebar-heading'> Survey </div><li class=\"nav-item\"> <a class=\"nav-link collapsed\" href='#' data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"true\" aria-controls=\"collapseTwo\"> <i class=\"flaticon flaticon-checklist\"></i> <span>Survey</span> </a> <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordionSidebar\"> <div class=\"bg-white py-2 collapse-inner rounded\"> <a class=\"collapse-item\" href='/SurveyHospital'>Hospitals</a><a class=\"collapse-item\" href='/ShowHealthCenterS'>Health Centers</a></div></div></li> <div class='text-center d-none d-md-inline'> </div></ul>");
                    }
                    else {
                        window.location.href="/";
                    }

                    $('#appusername').html(response.user.name);
                    $("#logout-link").click(function() {
                        // console.log(data);
                        $.ajax({
                            url: "../api/auth/logout",
                            dataType: "json",
                            type: "GET",
                            beforeSend : function( xhr ) {
                                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                            },
                            success: function (data, xhr) {
                                window.location.href = "/";
                                sessionStorage.clear();
                            },
                            error:function (data, xhr) {
                                console.log(xhr.responseText);
                            }
                        });
                    });
                    // $("#logout-link").click(function() {
                    //     // alert("logout");
                    //     $.ajax({
                    //         url: "../api/auth/logout",
                    //         dataType: "json",
                    //         type: "GET",
                    //         beforeSend: function (xhr) {
                    //             xhr.setRequestHeader('Authorization', 'BEARER ' + localStorage.token);
                    //         },
                    //         success: function (data, xhr) {
                    //             window.location.href = "../../";
                    //             localStorage.clear();
                    //         },
                    //         error:function (data, xhr,responseText) {
                    //             console.log( xhr.responseText);
                    //         }
                    //     });
                    // });
                }, error: function (xhr, status, error) {
                    if(xhr.status == 401){
//                        alert("Kindly login to proceed");
                        window.location.href="/";
                    }else{
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>'+error+'</p>');
                        console.log(xhr.responseText);
                    }
                    // console.log(xhr.responseText);
                }
//
            });
//            setInterval(request, 50000);
        });
        $(document).ready(function () {
            $("#logout-link").click(function() {
                // console.log(data);
                $.ajax({
                    url: "../api/auth/logout",
                    dataType: "json",
                    type: "GET",
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (data, xhr) {
                        window.location.href = "/";
                        sessionStorage.clear();
                    },
                    error:function (data, xhr) {
                        console.log(xhr.responseText);
                    }
                });
            });
        });
}



