<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemQuotationDays extends Model
{
    protected $table = "system_quotation_days";
    protected $fillable = ['id','quotation_id','agent_id','quotation_day'];
}
