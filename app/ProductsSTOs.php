<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsSTOs extends Model
{
    protected $table = "products_s_t_os";
    protected $fillable = ['id','products_id','products_agent_id','products_sto_price','provider_contract_id'];
}
