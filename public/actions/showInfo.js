$(document).ready(function () {
    var data  = sessionStorage.getItem('accessToken');
    if(!data == data){
        window.location.href = "/";
    }else{
        $(document).ready(function () {
            var url_string = location.href;
            var urlink = new URL(url_string);
            var id = urlink.searchParams.get("id");
            $.ajax({
                type: 'get',
                url: '../api/auth/ShowHospitalInfoMore/' + id +'',
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {
                    $('a[href$="/ShowHospitalInfoUI"]').addClass('active');
                    $('.img-profile').initial();
                    $.ajax({
                        type: 'get',
                        url: "../api/auth/DashboardCount",
                        dataType: 'json',
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                        },
                        success: function (response) {
                            JSON.stringify(response); //to string

                            jQuery('#countusers').html('0'+response.countusers);
                            jQuery('#countagent').html('0'+response.countagent);
                            jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                            jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                            jQuery('#countstandard').html('0'+response.countstandard);

                        }, error: function (xhr, status, error) {
                            jQuery('#login_error').show();
                            document.getElementById("login_error").style.display = "inherit";
                            jQuery('#login_error').append('<p>' + error + '</p>');
                        }
                        //
                    });
                    if(response.response_status == 400){
                    }
                    else {
                        console.log(response);
                        $(".hospital-profile").attr("data-name",response[0].facility_name);
                        $(".facilityName").html(response[0].facility_name);
                        $(".facilityPhone").html("+25" + response[0].facility_phone);
                        $(".province").html(response[0].name);
                        $(".district").html(response[0].district_name);
                        $(".hospitalCategory").html(response[0].hospital_cat_id);
                        $(".facilityEmail").html(response[0].facility_email);
                        $(".facilityHead").html(response[0].name_of_facility_head);
                        $(".alternatePhone").html(response[0].phone_alternative_facility_head);
                        $(".population").html(response[0].population_covered);
                        $(".beds").html(response[0].total_beds);
                        $(".maternityBeds").html(response[0].maternity_beds);
                        $(".ambulances").html(response[0].operational_ambulances);
                        $(".practitioner").html(response[0].general_practitioner);
                        $(".specialists").html(response[0].specialists);
                        $(".a1").html(response[0].a1);
                        $(".a2").html(response[0].a2);
                        $(".midWives").html(response[0].mid_wives);
                        $(".labTech").html(response[0].lab_techs);
                        $(".physio").html(response[0].physio);
                        $(".anesthetists").html(response[0].anesthetists);
                        $(".pharmacists").html(response[0].pharmacists);
                        $(".dentists").html(response[0].dentists);

                        $('.hospital-profile').initial();
                    }

                }, error: function (xhr, status, error) {
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>'+error+'</p>');
                }
//
            });
//            setInterval(request, 50000);
        });
        $(document).on('click', '.delete', function(){
            var id = $(this).attr("id");
            if(confirm("Are you sure you want to delete this records?"))
            {
                $.ajax({
                    type: 'post',
                    url: "../api/auth/DeleteHospitalInfo",
                    dataType: 'json',
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    data:{id:id},
                    success: function (response) {
                        JSON.stringify(response); //to string
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                        setTimeout(function() { window.location.reload() }, 3000);

                    }, error: function (xhr, status, error) {
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>' + response.response_message + '</p>');
//                    console.log(xhr.responseText);
                    }
                });
            }
        });
        $(document).on('click', '.edit-modal', function() {
            var id = $(this).data('id');
            $.ajax({
                type: 'get',
                url: '../api/auth/ShowEditHospitalInfo/' + id +'',
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {

                    JSON.stringify(response); //to string
//                    console.log(response);
                    $.each(response, function (index, value) {
                        $('#province_id_edit').append('' +
                            '<option value="'+ this.province_id +'" selected>'+ this.name +'</option>');
                    });
                    $.each(response, function (index, value) {
                        $('#district_id_edit').append('' +
                            '<option value="'+ this.district_id +'" selected>'+ this.district_name +'</option>');
                    });
                    $.each(response, function (index, value) {
                        $('#hospital_cat_id_edit').append('' +
                            '<option value="'+ this.hospital_cat_id +'" selected>'+ this.hospital_cat_id +'</option>');
                    });
                    $('.form-horizontal').show();
                    $('#id_edit').val(response[0].id );
                    $('#facility_name_edit').val(response[0].facility_name);
                    $('#facility_phone_edit').val(response[0].facility_phone);
                    $('#facility_email_edit').val(response[0].facility_email);
                    $('#name_of_facility_head_edit').val(response[0].name_of_facility_head);
                    $('#phone_of_head_facility_edit').val(response[0].phone_of_head_facility);
                    $('#name_alternative_facility_head_edit').val(response[0].name_alternative_facility_head);
                    $('#phone_alternative_facility_head_edit').val(response[0].phone_alternative_facility_head);
                    $('#population_covered_edit').val(response[0].population_covered);
                    $('#total_beds_edit').val(response[0].total_beds);
                    $('#maternity_beds_edit').val(response[0].maternity_beds);
                    $('#operational_ambulances_edit').val(response[0].operational_ambulances);
                    $('#general_practitioner_edit').val(response[0].general_practitioner);
                    $('#specialists_edit').val(response[0].specialists);
                    $('#a1_edit').val(response[0].a1);
                    $('#a2_edit').val(response[0].a2);
                    $('#mid_wives_edit').val(response[0].mid_wives);
                    $('#lab_techs_edit').val(response[0].lab_techs);
                    $('#physio_edit').val(response[0].physio);
                    $('#anesthetists_edit').val(response[0].anesthetists);
                    $('#pharmacists_edit').val(response[0].pharmacists);
                    $('#dentists_edit').val(response[0].dentists);

                }, error: function (xhr, status, error) {
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                }
            });
        });
        $(document).on('click', '#update_category', function () {
            $('#update_category').html('Updating..');
            var formData = $("#editModalForm").serialize();
            console.log(formData);
            $.ajax({
                type: "POST",
                url: "../api/auth/UpdateHospitalInfo",
                data: formData,
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                },
                success: function (response) {
                    JSON.stringify(response); //to string
                    console.log(response);
                    jQuery('#updating_error').show();
                    document.getElementById("updating_error").style.display = "inherit";
                    jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                    setTimeout(function() { window.location.reload() }, 2000);

                }, error: function (xhr, status, error) {
//                    jQuery('#updating_error').show();
//                    document.getElementById("updating_error").style.display = "inherit";
//                    jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                    console.log(xhr.responseText);
                }
            });

        });
    }
});