<?php

namespace App\Http\Controllers;


use App\Agents;

use App\Client;

use App\Country;
use App\ProductCategories;
use App\ProductProvider;
use App\Products;
use App\ProductsSTOs;
use App\ProviderContracts;

use App\Role;
use App\SystemQuotation;
use App\SystemQuotationDays;
use App\SystemQuoteDetails;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller

{
//    use Illuminate\Foundation\Testing\WithoutMiddleware;

    protected $user;

    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login']]);
        $this->middleware('jwt', ['except' => ['login']]);
    }

    /* create account */

    public function CreateAccount(Request $request){
        $User = new User();
        $User->email = $request['email'];
        $User->name = $request['name'];
        $User->password = bcrypt($request['password']);
        $User->save();

        return response()->json([
            'response_message' => "account successfully created",
        ]);


    }

    /*DASHBOARD COUNT*/

    public function DashboardCount(){
        $countusers = User::count();
        $countagent = Agents::count();

        return response()->json([
            'response_message' => "success",
            'response_status' =>200,
            'countusers' =>$countusers,
            'countagent' =>$countagent,
            'pendingQuotation' =>"",
            'approvedQuotation' =>"",

        ]);
    }
    /*SHOW ROLES*/

    public function Roles(){
        $listroles = Role::all();
        return $listroles->toArray();
    }


    public function ShowRoles(){
        $show = Role::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*LOGIN ROUTE FUNCTION*/

    public function login(){
        return view('backend.login');
    }

    /*SHOW ALL CREATED APP ACCOUNT */
    public function ShowSystemAccount(){
        $show = User::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW ACCOUNT BY EMAIL */
    public function ShowUserAccount(Request $request){
        $show = User::where('email',$request['email'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE USER ACCOUNT*/
    public function UpdateUserAccount(Request $request){
        $update = User::where('id',$request['id'])->update(['name' => $request['name'],'email'=>$request['email']]);
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($update){
            $arr = array('response_message' => 'Successfully update account information', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*LIST COUNTRIES */
    public function Countries(){
        $show = Country::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*ADD SYSTEM AGENT */
    public function AddAgent(Request $request){
    $all = $request->all();
    $add = Agents::create($all);

    $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
    if($add){
        $arr = array('response_message' => 'Successfully created a Agents', 'response_status' => true);
    }
    return Response()->json($arr);
    }

    /*SHOW ALL AGENTS*/

    public function ShowAgent(){
        $show = Agents::join('apps_countries', 'apps_countries.id', '=', 'agents.agent_country')
            ->select('agents.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "You do not have any Agents yet",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW AGENT BY ID*/
    public function ShowAgentById(Request $request){
        $show = Agents::where('agents.id',$request['id'])
            ->join('apps_countries', 'apps_countries.id', '=', 'agents.agent_country')
            ->select('agents.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE AGENT INFORMATION */
    public function UpdateAgent(Request $request){
        $id = $request['id_edit'];
        $update = Agents::find($id);
        $update->agent_name = $request['agent_name'];
        $update->agent_email = $request['agent_email'];
        $update->agent_phone = $request['agent_phone'];
        $update->agent_percentage = $request['agent_percentage'];
        $update->agent_country = $request['agent_country'];
        $update->agent_city = $request['agent_city'];
        $update->agent_state = $request['agent_state'];
        $update->agent_address = $request['agent_address'];
        $update->agent_notes = $request['agent_notes'];
        $update->save();

        return response()->json([
            'response_message' =>'successfully updated Agents information',
            'response_status' =>true
        ]);
    }
    /*DELETE AGENT ACCOUNT */
    public function DeleteAgent(Request $request){
        $id = $request['id'];
        $delete = Agents::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted Agents', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    public function AddClients(Request $request){
        $all = $request->all();
        $add = Client::create($all);

        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully created a client', 'response_status' => true);
        }
        return Response()->json($arr);
    }
    public function ShowClients(){
        $show = Client::join('apps_countries', 'apps_countries.id', '=', 'clients.client_country')
            ->select('clients.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "You do not have any client yet",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }
    public function ShowClientsById(Request $request){
        $show = Client::where('clients.id',$request['id'])
            ->join('apps_countries', 'apps_countries.id', '=', 'clients.client_country')
            ->select('clients.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }
    public function UpdateClient(Request $request){
        $id = $request['id_edit'];
        $update = Client::find($id);
        $update->client_name = $request['client_name'];
        $update->client_email = $request['client_email'];
        $update->client_phone = $request['client_phone'];
        $update->client_website = $request['client_website'];
        $update->client_country = $request['client_country'];
        $update->client_city = $request['client_city'];
        $update->client_state = $request['client_state'];
        $update->client_address = $request['client_address'];
        $update->client_notes = $request['client_notes'];
        $update->save();

        return response()->json([
            'response_message' =>'successfully updated client information',
            'response_status' =>true
        ]);
    }
    public function DeleteClient(Request $request){
        $id = $request['id'];
        $delete = Client::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted client', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*ADD PRODUCT CATEGORY*/
    public function AddProductCategory(Request $request){
        $all = $request->all();
        $add = ProductCategories::create($all);

        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully added a service name', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*SHOW PRODUCT CATEGORIES */
    public function ShowProductCategory(){
        $show = ProductCategories::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }
    /*SHOW PRODUCT CATEGORY BY ID */
    public function ShowProductCategoryById(Request $request){
        $show = ProductCategories::where('id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE PRODUCT CATEGORY */
    public function UpdateProductCategory(Request $request){
        $id = $request['id'];
        $update = ProductCategories::find($id);
        $update->product_categories_name = $request['product_categories_name'];
        $update->save();
        return response()->json([
            'response_message' =>'successfully updated service name',
            'response_status' =>true
        ]);
    }

    /*DELETE PRODUCT CATEGORY */
    public function DeleteProductCategory(Request $request){
        $id = $request['id'];
        $delete = ProductCategories::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted service name', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    /*ADD PRODUCT PROVIDER */
    public function AddProductProvider(Request $request){
        $all = $request->all();
        $add = ProductProvider::create($all);

        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully added provider name', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*SHOW PRODUCT PROVIDER */
    public function ShowProductProvider(){
//        $show = ProductProvider::all();
        $show = ProductProvider::join('product_categories', 'product_categories.id', '=', 'product_providers.product_category_id')
            ->select('product_providers.*','product_categories.product_categories_name')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW PRODUCT PROVIDER BY ID */
    public function ShowProductProviderById(Request $request){
        $show = ProductProvider::where('id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }


    /*SHOW PROVIDER BY CATEGORY */
    public function ShowProviderByCategory(Request $request){
        $id = $request['id'];
        $show = ProductProvider::where('product_category_id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE PRODUCT PROVIDER*/
    public function UpdateProductProvider(Request $request){
        $id = $request['id_edit'];
        $update = ProductProvider::find($id);
        $update->provider_name = $request['provider_name'];
        $update->save();
        return response()->json([
            'response_message' =>'successfully updated provider name',
            'response_status' =>true
        ]);
    }

    /*DELETE PRODUCT PROVIDER*/
    public function DeleteProductProvider(Request $request){
        $id = $request['id'];
        $delete = ProductProvider::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted provider name', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*ADD CONTRACT */
    public function AddContract(Request $request){
        $all = $request->all();
        $add = ProviderContracts::create($all);
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully added a contract', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    /*SHOW CONTRACT */
    public function ShowContract(){
        $show = ProviderContracts::join('product_categories', 'product_categories.id', '=', 'provider_contracts.product_category_id')
            ->join('product_providers', 'product_providers.id', '=', 'provider_contracts.provider_id')
            ->select('provider_contracts.*','product_categories.product_categories_name','product_providers.provider_name')->get();

        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW CONTRACT BY PROVIDER*/
    public function ShowContractByProvider(Request $request){
        $id = $request['id'];
        $show = ProviderContracts::where('product_category_id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW CONTRACT PRICE*/
    public function ShowContractPrice(Request $request){
        $contract_id = $request['contract_id'];
        $agent_id = $request['agent_id'];

        $show = ProductsSTOs::where('provider_contract_id',$contract_id)
            ->where('products_agent_id',$agent_id)->get();

        foreach($show as $data){
            $this->ContractPriceRackRate($data);
            $data['Rackrate'] = $this->ContractPriceRackRate($data);
        }

        return response()->json($show);
    }


    public function ContractPriceRackRate($data){
        $rackrate = Products::where('id',$data->products_id)->value('product_price');
        $new = (int)$rackrate;
        return json_encode($new);
    }



    /*ADD PRODUCT */
    public function AddProduct(Request $request){
        $all = $request->all();
        $add = Products::create($all);
        $last_id = $add->id;
        foreach ($request['products_agent_id'] as $key => $value){
            $add_finding = new ProductsSTOs();
            $add_finding->products_agent_id = $request['products_agent_id'][$key];
            $add_finding->products_sto_price = $request['products_sto_price'][$key];
            $add_finding->provider_contract_id = $request['provider_contract_id'];
            $add_finding->products_id = $last_id;
            $add_finding->save();
        }
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully added a product', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*SHOW PRODUCT */
    public function ShowProduct(){
//        $show = Products::all();
        $show = Products::join('product_categories', 'product_categories.id', '=', 'products.product_category_id')
            ->join('product_providers', 'product_providers.id', '=', 'products.provider_id')
            ->join('provider_contracts', 'provider_contracts.id', '=', 'products.provider_contract_id')
            ->select('products.*','provider_contracts.provider_contract_name','product_categories.product_categories_name','product_providers.provider_name')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }


    /*SHOW PRODUCT BY ID */
    public function ShowProductById(Request $request){
        $show = Products::where('products.id',$request['id'])
            ->join('product_categories', 'product_categories.id', '=', 'products.product_category_id')
            ->join('product_providers', 'product_providers.id', '=', 'products.provider_id')
            ->select('products.*','product_categories.product_categories_name','product_providers.provider_name')->get();
//        $show = Products::where('id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE PRODUCT*/
    public function UpdateProduct(Request $request){
        $id = $request['id_edit'];
        $update = Products::find($id);
        $update->product_name = $request['product_name'];
        $update->product_price = $request['product_price'];
        $update->save();
        return response()->json([
            'response_message' =>'successfully updated product information',
            'response_status' =>true
        ]);
    }

    /*DELETE PRODUCTS*/
    public function DeleteProduct(Request $request){
        $id = $request['id'];
        $delete = Products::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted product', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    /*ADD QUOTATION */
    public function AddQuotation(Request $request){
        $all = $request->all();
        $add = SystemQuotation::create($all);
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($add){
            $arr = array('response_message' => 'Successfully added a Quotation', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    /*SHOW QUOTATION */
    public function ShowQuotation(){
        $show = SystemQuotation::join('agents', 'agents.id', '=', 'system_quotations.agent_id')
            ->select('system_quotations.*','agents.agent_name')->get();
//        $show = Products::where('id',$request['id'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*DELETE QUOTATION */
    public function DeleteQuotation(Request $request){
        $id = $request['id'];
        $delete = SystemQuotation::find($id);
        $delete->delete();
        $delete_service = SystemQuoteDetails::where('system_quotation_day_id',$id)->delete();
        $delete_quotation_day= SystemQuotationDays::where('quotation_id',$id)->delete();

        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted quotation', 'response_status' => true);
        }
        return Response()->json($arr);
    }


    /*SHOW AGENT BY CLIENT ????*/
    public function AgentShowClients(){
        $show = Client::select('id','client_name')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW AGENT DETAILS */
    public function AgentShowAgents(){
        $show = Agents::select('id','agent_name')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW PRODUCT BY AGENT */
    public function AgentShowProducts(){
        $show = Products::select('id','product_name','product_price')->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW QUOTATION BY ID */
    public function ShowQuotationAgentId(Request $request){
        $id = $request['id'];
        $show = SystemQuotation::where('id',$id)->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW QUOTATION DAYS BY AGENT ID */
    public function ShowQuotationDaysAgentId(Request $request){
        $id = $request['id'];
        $show = SystemQuotationDays::where('id',$id)->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }


    /*ADD DAYS TO QUOTATION */
    public function AddQuotationDays(Request $request){
        $all = $request->all();
        $check_days = SystemQuotationDays::where('quotation_id',$request['quotation_id'])->get();

        if(0 == count($check_days)){

            $getlastdate = SystemQuotation::where('id',$request['quotation_id'])->value('travel_start_date');
            $addquotday = new SystemQuotationDays();

            $addquotday->quotation_id = $request['quotation_id'];
            $addquotday->agent_id = $request['agent_id'];
            $addquotday->quotation_day = $getlastdate;
            $addquotday->save();
            $last_id = $addquotday->id;

            foreach ($request['product_category_id'] as $key => $value){
            $rack_rate = $request['rack_rate'][$key];
            $quantity = $request['quantity'][$key];
            $total_rack_rate = $rack_rate * $quantity;
            $add_finding = new SystemQuoteDetails();

            $add_finding->product_category_id = $request['product_category_id'][$key];
            $add_finding->provider_id = $request['provider_id'][$key];
            $add_finding->provider_contract_id = $request['provider_contract_id'][$key];
            $add_finding->rack_rate = $request['rack_rate'][$key];
            $add_finding->unit_cost = $request['unit_cost'][$key];
            $add_finding->total_price = $request['total_price'][$key];
            $add_finding->total_rack_rate = $total_rack_rate;
            $add_finding->quantity = $request['quantity'][$key];
            $add_finding->system_quotation_day_id = $last_id;
            $add_finding->system_quotation_id = $request['quotation_id'];
            $add_finding->save();
            }

            $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
            if($addquotday){
                $arr = array('response_message' => 'Successfully added a day', 'response_status' => true);
            }
            return Response()->json($arr);

        }else{

            $check_days = SystemQuotationDays::where('quotation_id',$request['quotation_id'])->get()->last();
            $date = strtotime("+1 day", strtotime($check_days->quotation_day));
            $new_date = date("Y-m-d", $date);

            $addquotday = new SystemQuotationDays();
            $addquotday->quotation_id = $request['quotation_id'];
            $addquotday->agent_id = $request['agent_id'];
            $addquotday->quotation_day = $new_date;
            $addquotday->save();
            $last_id = $addquotday->id;

            foreach ($request['product_category_id'] as $key => $value){
                $rack_rate = $request['rack_rate'][$key];
                $quantity = $request['quantity'][$key];
                $total_rack_rate = $rack_rate * $quantity;
                $add_finding = new SystemQuoteDetails();
                $add_finding->product_category_id = $request['product_category_id'][$key];
                $add_finding->provider_id = $request['provider_id'][$key];
                $add_finding->provider_contract_id = $request['provider_contract_id'][$key];
                $add_finding->rack_rate = $request['rack_rate'][$key];
                $add_finding->unit_cost = $request['unit_cost'][$key];
                $add_finding->total_price = $request['total_price'][$key];
                $add_finding->total_rack_rate = $total_rack_rate;
                $add_finding->quantity = $request['quantity'][$key];
                $add_finding->system_quotation_day_id = $last_id;
                $add_finding->system_quotation_id = $request['quotation_id'];
                $add_finding->save();
            }
            $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
            if($addquotday){
                $arr = array('response_message' => 'Successfully added a day', 'response_status' => true);
            }
            return Response()->json($arr);
        }

    }


    /*SHOW QUOTATION DAYS */
    public function ShowQuotationDays(Request $request){

        $show = SystemQuotationDays::where('system_quotation_days.quotation_id',$request['id'])
            ->join('system_quotations', 'system_quotations.id', '=', 'system_quotation_days.quotation_id')
            ->join('agents', 'agents.id', '=', 'system_quotation_days.agent_id')
            ->select('system_quotation_days.*','system_quotations.quotation_name','agents.agent_name')
            ->get();

        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }


    /*SHOW QUOTATION DETAILS */
    public function ShowQuotationDetails(Request $request){
        $id = $request['id'];
//        $show = SystemQuoteDetails::where('system_quotation_day_id',$request['id'])->get();

        $show = SystemQuoteDetails::where('system_quote_details.system_quotation_day_id',$request['id'])
            ->join('product_categories', 'product_categories.id', '=', 'system_quote_details.product_category_id')
            ->join('product_providers', 'product_providers.id', '=', 'system_quote_details.provider_id')
            ->join('provider_contracts', 'provider_contracts.id', '=', 'system_quote_details.provider_contract_id')
            ->select('system_quote_details.*','provider_contracts.provider_contract_name','product_categories.product_categories_name','product_providers.provider_name')->get();

        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*DELETE QUOTATION DAYS */
    public function DeleteQuotationDays(Request $request){
        $id = $request['id'];
        $delete = SystemQuotationDays::find($id);
        $delete->delete();
        $delete_service = SystemQuoteDetails::where('system_quotation_day_id',$id)->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted a day', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*ADD MORE DAYS TO QUOTATION */
    public function AddMoreQuotationDays(Request $request){
        foreach ($request['product_category_id'] as $key => $value){
            $add_finding = new SystemQuoteDetails();

            $add_finding->product_category_id = $request['product_category_id'][$key];
            $add_finding->provider_id = $request['provider_id'][$key];
            $add_finding->provider_contract_id = $request['provider_contract_id'][$key];
            $add_finding->rack_rate = $request['rack_rate'][$key];
            $add_finding->unit_cost = $request['unit_cost'][$key];
            $add_finding->total_price = $request['total_price'][$key];
            $add_finding->quantity = $request['quantity'][$key];
            $add_finding->system_quotation_day_id = $request['system_quotation_day_id'];
            $add_finding->system_quotation_id = $request['quotation_id'];
            $add_finding->save();
            $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
            if($add_finding){
                $arr = array('response_message' => 'Successfully added services', 'response_status' => true);
            }
            return Response()->json($arr);
        }
    }

    /*DELETE DAYS ON QUOTATION */
    public function DeleteMoreQuotationDays(Request $request){
        $delete = SystemQuoteDetails::find($request['id']);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted a service', 'response_status' => true);
        }
        return Response()->json($arr);

    }


    /*GENERATE PDF FOR CLIENT */
    public function GeneratePdfClient(Request $request){
        $getagent_id = SystemQuotation::where('id',$request['id'])->value('agent_id');
        $show = Agents::where('agents.id',$getagent_id)
            ->join('apps_countries', 'apps_countries.id', '=', 'agents.agent_country')
            ->select('agents.*','apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*GENERATE PDF */
    public function GeneratePdf(Request $request){
        $show = SystemQuotationDays::where('system_quotation_days.quotation_id',$request['id'])
            ->join('system_quotations', 'system_quotations.id', '=', 'system_quotation_days.quotation_id')
            ->join('agents', 'agents.id', '=', 'system_quotation_days.agent_id')
            ->select('system_quotation_days.*','system_quotations.quotation_name','agents.agent_name')
            ->get();
        $unit_cost = SystemQuoteDetails::where('system_quotation_id',$request['id'])->sum('unit_cost');
        $total_price = SystemQuoteDetails::where('system_quotation_id',$request['id'])->sum('total_price');
        $rack_rate = SystemQuoteDetails::where('system_quotation_id',$request['id'])->sum('rack_rate');
        $total_rack_rate= SystemQuoteDetails::where('system_quotation_id',$request['id'])->sum('total_rack_rate');

        foreach ($show as $datas){
            $this->GeneratePdfServices($datas);
            $datas['Records'] = $this->GeneratePdfServices($datas);
//            $datas['Total'] = $this->GeneratePdfTotal($datas);
        }

        $arr = array(
            'GeneratePdf' => $show,
            'TotalPdf' => [
                'unit_cost' => $unit_cost,
                'total_price' => $total_price,
                'rack_rate' => $rack_rate,
                'total_rack' => $total_rack_rate,
            ],
        );

        return $arr;

    }

    /*GENERATE PDF BY SERVICES*/
    public function GeneratePdfServices($datas){
        $show = SystemQuoteDetails::where('system_quote_details.system_quotation_day_id',$datas->id)
            ->join('product_categories', 'product_categories.id', '=', 'system_quote_details.product_category_id')
            ->join('product_providers', 'product_providers.id', '=', 'system_quote_details.provider_id')
            ->join('provider_contracts', 'provider_contracts.id', '=', 'system_quote_details.provider_contract_id')
            ->select('system_quote_details.*','provider_contracts.provider_contract_name','product_categories.product_categories_name','product_providers.provider_name')->get();

        return $show->toArray();
    }


}
