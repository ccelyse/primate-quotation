<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'BackendController@login');


//Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {
//    Route::get('dashboard', 'TestingController@dashboard');
//});

Route::group([

//    'middleware' => 'role:admin,guard:api',
//    'prefix' => 'admin',

], function () {
    Route::get('dashboard', 'TestingController@dashboard');
    Route::get('auth', 'TestingController@auth');
    Route::get('CreateAccount', 'TestingController@CreateAccount');
    Route::get('HospitalAdd', 'TestingController@HospitalAdd');
    Route::get('ShowHospitalInfoUI', 'TestingController@ShowHospitalInfoUI');
    Route::get('ShowHospitalInfoMore', 'TestingController@ShowHospitalInfoMore');

// Hospital/Health Categorization Backend Route

    Route::get('H_Categorization', 'TestingController@H_Categorization');

// Health Center Backend Route

    Route::get('Health_Center_Add', 'TestingController@Health_Center_Add');
    Route::get('ShowHealthCenterUI', 'TestingController@ShowHealthCenterUI');

// Accreditation Surveyors Backend Route
    Route::get('AccreditationSurveyors', 'TestingController@AccreditationSurveyors');
// Risk Area Backend Route
    Route::get('RiskArea', 'TestingController@RiskArea');
// Standard Backend Route
    Route::get('Standard', 'TestingController@Standard');
// Level Backend Route
    Route::get('Level', 'TestingController@Level');
// Perform Finding Backend Route
    Route::get('Perform_finding', 'TestingController@Perform_finding');

// AccreditxSurvey Backend Route
    Route::get('AccreditxSurvey', 'TestingController@AccreditxSurvey');


// Hospital Backend Route
    Route::get('HDashboard', 'TestingController@HDashboard');
    Route::get('SystemAccount', 'TestingController@SystemAccount');
    Route::get('ShowHospital', 'TestingController@ShowHospital');
    Route::get('HospitalProfile', 'TestingController@HospitalProfile');

    // Assessment Route
    Route::get('AssessmentPeriod', 'TestingController@AssessmentPeriod');

//  Clients Backend Route
    Route::get('SystemClients', 'TestingController@SystemClients');
    Route::get('SystemProducts', 'TestingController@SystemProducts');
    Route::get('SystemAgents', 'TestingController@SystemAgents');
    Route::get('SystemContracts', 'TestingController@SystemContracts');
    Route::get('SystemQuotation', 'TestingController@SystemQuotation');
    Route::get('SystemProductCategories', 'TestingController@SystemProductCategories');
    Route::get('SystemProductProvider', 'TestingController@SystemProductProvider');
//    Route::get('SystemPdf', 'TestingController@SystemPdf');
    Route::get('SystemPdf/', 'TestingController@SystemPdf');

    Route::get('SystemQuotationDays', 'TestingController@SystemQuotationDays');
    Route::get('SystemQuotationDaysServices', 'TestingController@SystemQuotationDaysServices');
    Route::get('CheckNewDate', 'TestingController@CheckNewDate');

    Route::get('SurveyHospital', 'TestingController@SurveyHospital');
    Route::get('ShowHospitalInfoMoreS', 'TestingController@ShowHospitalInfoMoreS');
    Route::get('ShowHealthCenterS', 'TestingController@ShowHealthCenterS');
    Route::post('UploadExcelHospital', 'TestingController@UploadExcelHospital');

    //    Survey reports
    Route::get('SurveyReports', 'TestingController@SurveyReports');
    Route::get('AccreditationReport', 'TestingController@AccreditationReport');
    Route::get('AccreditationReportPdf', 'TestingController@AccreditationReportPdf');
});

Route::group([

//    'middleware' => 'role:admin,guard:api',
    'prefix' => 'hospital',

], function () {

});

//Route::prefix('admin')->group( function () {
//
////Route::get('dashboard', 'BackendC ontroller@dashboard');
//    Route::get('dashboard', 'TestingController@dashboard');
//    Route::get('CreateAccount', 'TestingController@CreateAccount');
//    Route::get('HospitalAdd', 'TestingController@HospitalAdd');
//    Route::get('ShowHospitalInfoUI', 'TestingController@ShowHospitalInfoUI');
//    Route::get('ShowHospitalInfoMore', 'TestingController@ShowHospitalInfoMore');
//
//// Hospital/Health Categorization Backend Route
//
//    Route::get('H_Categorization', 'TestingController@H_Categorization');
//
//// Health Center Backend Route
//
//    Route::get('Health_Center_Add', 'TestingController@Health_Center_Add');
//    Route::get('ShowHealthCenterUI', 'TestingController@ShowHealthCenterUI');
//
//// Accreditation Surveyors Backend Route
//    Route::get('AccreditationSurveyors', 'TestingController@AccreditationSurveyors');
//// Risk Area Backend Route
//    Route::get('RiskArea', 'TestingController@RiskArea');
//// Standard Backend Route
//    Route::get('Standard', 'TestingController@Standard');
//// Level Backend Route
//    Route::get('Level', 'TestingController@Level');
//// Perform Finding Backend Route
//    Route::get('Perform_finding', 'TestingController@Perform_finding');
//// AccreditxSurvey Backend Route
//    Route::get('AccreditxSurvey', 'TestingController@AccreditxSurvey');
//});


