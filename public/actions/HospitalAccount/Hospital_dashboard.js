var data  = sessionStorage.getItem('accessToken');
if(!data == data){
    window.location.href = "/";
}else {

    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                $.ajax({
                    type: 'get',
                    url: "../api/auth/DashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string
                        console.log(response);
                        $('.img-profile').initial();
                        $('a[href$="#"]').addClass('active');
                        jQuery('#countusers').html('0'+response.countusers);
                        jQuery('#countagent').html('0'+response.countagent);
                        jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                        jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                        jQuery('#countstandard').html('0'+response.countstandard);
                    },error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
                $.ajax({
                    type: 'post',
                    url: "../api/auth/HospitalDashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                    },
                    data:{
                        id:response.user.allhospital,
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string
                        jQuery('#countPopulation').html('0'+response.population_covered);
                        jQuery('#countOperational').html('0'+response.operational_ambulances);
                        jQuery('#countPharmacists').html('0'+response.pharmacists);
                        jQuery('#countDentists').html('0'+response.dentists);

                    }, error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
        });
    });
}
