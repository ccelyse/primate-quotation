@extends('backend.layout.master')

@section('title', 'Primate safaris')

@section('content')

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5 ">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Primate safaris</h1>
                                    </div>

                                    <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none">

                                    </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember Me</label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block" id="login-form">Login</button>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    {{--<script--}}
            {{--src="https://code.jquery.com/jquery-3.3.1.js"--}}
            {{--integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="--}}
            {{--crossorigin="anonymous"></script>--}}
    <script src="js/jquery-3.3.1.js" type="application/javascript"></script>

<script type="application/javascript">

    $(document).on('click', '#login-form', function() {
                var email = $('#email').val();
                var password = $('#password').val();

               $.ajax({
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                    type: "POST",
                    url: "../api/auth/login",
                   data: {
                       'email': email,
                       'password': password,
                    },
                   success: function (response) {
                       JSON.stringify(response); //to string
//                       console.log(response);
                       window.sessionStorage.accessToken = response.access_token;
                       {{--window.location.href = '{{ url('admin/dashboard')}}';--}}
                       window.location = 'auth';
//                       alert(response.access_token);

                   }, error: function (xhr, status, error) {
//                      console.log(error);
                       jQuery('#login_error').show();
                       document.getElementById("login_error").style.display = "inherit";
                       jQuery('#login_error').append('<p>Email or password is not matching</p>');
                   }
                });

    });
</script>
@endsection
