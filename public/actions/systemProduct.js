var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {

    $(document).on('click', '#add_product', function() {
        $('#add_product').html('Adding....');
        var formData = $("#AddProducts").serialize();
        // console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/AddProduct",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(data, xhr, response) {
                JSON.stringify(response); //to string
                jQuery('#added_error').show();
                document.getElementById("added_error").style.display = "inherit";
                jQuery('#added_error').append('<p>' + data.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);
            },
            error: function(xhr, status, error) {
                // jQuery('#login_error').show();
                // document.getElementById("login_error").style.display = "inherit";
                // jQuery('#login_error').append('<p> ' + data.message + '</p>');
                console.log(xhr.responseText);
            }

        });

    });
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $(function(){
                    var current = location.pathname;
                    $('#sidebarmenu a').each(function(){
                        var $this = $(this);
                        // if the current path is like this link, make it active
                        if($this.attr('href').indexOf(current) !== -1){
                            $this.addClass('active');
                        }
                    })
                });
                $.ajax({
                    type: 'get',
                    url: "../api/auth/DashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string

                        jQuery('#countusers').html('0'+response.countusers);
                        jQuery('#countagent').html('0'+response.countagent);
                        jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                        jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                        jQuery('#countstandard').html('0'+response.countstandard);

                    }, error: function (xhr, status, error) {
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>' + error + '</p>');
                    }
                    //
                });
                $('.img-profile').initial();
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
//
        });
    });
    $.ajax({
        type: 'get',
        url: "../api/auth/ShowProduct",
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'BEARER ' + data);
        },
        success: function(response) {
            if (response.response_status == 400) {
                $('#Categorization_info').append('');
            } else {

                var $i = 1;
                $.each(response, function (key, value) {
                    $("#Accreditation_info").append("" +
                        "<tr><td class='text-center'>" + $i++ +"</td>" +
                        '<td>' + this.provider_name + '</td>' +
                        '<td>' + this.product_categories_name + '</td>' +
                        '<td>' + this.provider_contract_name + '</td>' +
                        '<td>$' + this.product_price + '</td>' +
                        '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                        '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        '</tr>');
                });
                /*
                |--------------------------------------------
                | Load Data Table
                |--------------------------------------------
                */
                var table = $('#provincesTable').DataTable();
            }

        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
        //
    });

    $(document).ready(function() {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowProductCategory",
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response); //to string
                $.each(response, function(index, value) {
                    $('.ProductsService').append('' +
                        '<option value="' + this.id + '">' + value.product_categories_name + '</option>');
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
            //
        });
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowAgent",
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response); //to string
                $.each(response, function(index, value) {
                    $('#AgentShowProducts').append('' +
                        '<option value="' + this.id + '">' + value.agent_name + '</option>');
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
            //
        });
    });
    $(document).ready(function() {
        $(".note-popover").remove();
        var i=1;
        $('#add').click(function(){
            i++;
            // $('.multi-field-wrapper').append('<div class="row multi-field'+i+'" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;"> <div class=\'col-lg-6\'><div class="form-group"> <select class="form-control form-control-user" name="client_country" id="Products_inc'+i+'" required> <option value="">Products</option> </select> </div></div>  <div class=\'col-lg-6\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="agent_name" id="Price_inc'+i+'" placeholder="Price" required> </div></div> <div class=\'col-lg-12\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="agent_name" id="Quantity_inc'+i+'" placeholder="Quantity" required> </div></div><button type="button" class="remove-field btn btn-danger btn-circle" id="'+i+'" style="margin-left: 10px; margin-bottom: 10px;"><i class="fas fa-trash"></i> </button></div>');
            $('.multi-field-wrapper').append('<div class="row multi-field'+i+'" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;">  <div class=\'col-lg-6\'> <div class="form-group"> <select class="form-control form-control-user" name="products_agent_id[]" id="AgentShowProducts'+i+'" required> <option value="">Select Agent</option> </select> </div></div><div class=\'col-lg-6\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="products_sto_price[]" id="Price_inc'+i+'" placeholder="STO RATES" required> </div></div><button type="button" class="remove-field btn btn-danger btn-circle" id="'+i+'" style="margin-left: 10px; margin-bottom: 10px;"><i class="fas fa-trash"></i> </button></div>');
            $.ajax({
                type: 'get',
                url: "../api/auth/ShowAgent",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {
                    JSON.stringify(response); //to string
                    $.each(response, function(index, value) {
                        $('#AgentShowProducts'+i+'').append('' +
                            '<option value="' + this.id + '">' + value.agent_name + '</option>');
                    });
                }, error: function (xhr, status, error) {
                    if(xhr.status == 401){
                        window.location.href="/";
                    }else{
                        console.log(xhr.responseText);
                    }
                }
            });
        });
        $(document).on('click', '.remove-field', function(){
            var button_id = $(this).attr("id");
            $('.multi-field'+button_id+'').remove();
        });
    });
    $(document).on('click', '.edit-modal', function() {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: '../api/auth/ShowProductById/' + id + '',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {

                JSON.stringify(response); //to string
                $('.form-horizontal').show();
                $('#id_edit').val(response[0].id);
                // $('#product_category_id_edit').val(response[0].product_categories_name);
                // $('#provider_id_edit').val(response[0].provider_name);
                $('#product_price_edit').val(response[0].product_price);

                $.each(response, function(index, value) {
                    $('#product_category_id_edit').append('' +
                        '<option value=' + this.id + ' selected>' + value.product_categories_name + '</option>');
                });
                $.each(response, function(index, value) {
                    $('#provider_id_edit').append('' +
                        '<option value=' + this.id + ' selected>' + value.provider_name + '</option>');
                });

            },
            error: function(xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });
    });
    $('.ProductsService').on('change', function() {
        var id = $('.ProductsService').val();
        $.ajax({
            type: "post",
            url: "../api/auth/ShowProviderByCategory",
            data:{
                id:id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response);
                $.each(response, function (index, value) {
                    $(".Products_Provider").empty();
                    $(".Products_Provider").append("<option value=''>Select Provider</option>");
                    $(response).each(function(i){
                        $(".Products_Provider").append("<option value="+response[i].id+">"+response[i].provider_name+"</option>");
                    });
                });

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $('.Products_Provider').on('change', function() {
        var id = $('.Products_Provider').val();
        $.ajax({
            type: "post",
            url: "../api/auth/ShowContractByProvider",
            data:{
                id:id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response);
                $.each(response, function (index, value) {
                    $(".Products_Contract").empty();
                    $(".Products_Contract").append("<option value=''>Select Contract</option>");
                    $(response).each(function(i){
                        $(".Products_Contract").append("<option value="+response[i].id+">"+response[i].provider_contract_name+"</option>");
                    });
                });

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
//     $(document).on('click', '#update_provider', function() {
//         $('#update_provider').html('Updating..');
//         var formData = $("#EditProvider").serialize();
//         $.ajax({
//             type: "POST",
//             url: "../api/auth/UpdateProductProvider",
//             data: formData,
//             dataType: 'json',
//             beforeSend: function(xhr) {
//                 xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
//             },
//             success: function(response) {
//                 JSON.stringify(response);
//                 console.log(response.response_message);
//                 jQuery('#updating_error').show();
//                 document.getElementById("updating_error_edit").style.display = "block";
//                 jQuery('#updating_error_edit').append('<p>' + response.response_message + '</p>');
//                 setTimeout(function() {
//                     window.location.reload()
//                 }, 2000);
//
//             },
//             error: function(xhr, status, error) {
//                 console.log(xhr.responseText);
//             }
//         });
//
//     });
//     $(document).on('click', '.delete', function() {
//         var id = $(this).attr("id");
// //            alert(id);
//         if (confirm("Are you sure you want to delete this records?")) {
//             $.ajax({
//                 type: 'post',
//                 url: "../api/auth/DeleteProductProvider",
//                 dataType: 'json',
//                 beforeSend: function(xhr) {
//                     xhr.setRequestHeader('Authorization', 'BEARER ' + data);
//                 },
//                 data: {
//                     id: id
//                 },
//                 success: function(response) {
//                     JSON.stringify(response); //to string
//                     jQuery('#delete_error').show();
//                     document.getElementById("delete_error").style.display = "block";
//                     jQuery('#delete_error').append('<p>' + response.response_message + '</p>');
//                     setTimeout(function() {
//                         window.location.reload()
//                     }, 2000);
//
//                 },
//                 error: function(xhr, status, error) {
// //                        jQuery('#login_error').show();
// //                        document.getElementById("login_error").style.display = "inherit";
// //                        jQuery('#login_error').append('<p>' + response.response_message + '</p>');
//                     console.log(xhr.responseText);
//                 }
//             });
//         }
//     });
    
}
