<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = "clients";
    protected $fillable = ['id','client_number','client_name','client_email','client_phone','client_website','client_country',
        'client_city','client_state','client_address','client_notes'];
}
