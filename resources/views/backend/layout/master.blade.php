<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="../../backend/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="../backend/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../backend/app-assets/css/custom-font.css">
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="../css/flaticon.css">
    <script src="../../formwizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="../../formwizard/js/jquery.validate.min.js"></script>
    {{--<script src="../backend/vendor/jquery/jquery.min.js"></script>--}}
    <script src="../../backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script src="../../../js/initial.min.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
</head>

<body>

@yield('content')


</body>

</html>
