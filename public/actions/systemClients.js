var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {
    $.ajax({
        type: 'get',
        url: "../api/auth/Countries",
        dataType: 'json',
        beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
        },
        success: function (response) {
            JSON.stringify(response); //to string
            $.each(response, function(index, value) {
                $('#client_country').append('' +
                    '<option value=' + this.id + '>' + value.country_name + '</option>');

                $('#client_country_edit').append('' +
                    '<option value=' + this.id + '>' + value.country_name + '</option>');

            });
        }, error: function (xhr, status, error) {
            if(xhr.status == 401){
                window.location.href="/";
            }else{
                console.log(xhr.responseText);
            }
        }
//
    });
    $('#client_number').val('CLI-'+Math.floor(Math.random() * 1000));
    $(document).on('click', '#add_clients', function() {
        $('#add_clients').html('Adding....');
        var formData = $("#AddClients").serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/AddClients",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(data, xhr, response) {
                JSON.stringify(response); //to string
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + data.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);
            },
            error: function(xhr, status, error) {
                // jQuery('#login_error').show();
                // document.getElementById("login_error").style.display = "inherit";
                // jQuery('#login_error').append('<p> ' + data.message + '</p>');
                console.log(xhr.responseText);
            }

        });

    });
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $('#id_hosp').val(response.user.id);
                $('#user_edit').val(response.user.id);

                $.ajax({
                    type: 'get',
                    url: "../api/auth/ShowClients",
                    dataType: 'json',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                    },
                    success: function(response) {
                        $(function(){
                            var current = location.pathname;
                            $('#sidebarmenu a').each(function(){
                                var $this = $(this);
                                // if the current path is like this link, make it active
                                if($this.attr('href').indexOf(current) !== -1){
                                    $this.addClass('active');
                                }
                            })
                        });
                        $.ajax({
                            type: 'get',
                            url: "../api/auth/DashboardCount",
                            dataType: 'json',
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                            },
                            success: function (response) {
                                JSON.stringify(response); //to string

                                jQuery('#countusers').html('0'+response.countusers);
                                jQuery('#countagent').html('0'+response.countagent);
                                jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                                jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                                jQuery('#countstandard').html('0'+response.countstandard);

                            }, error: function (xhr, status, error) {
                                jQuery('#login_error').show();
                                document.getElementById("login_error").style.display = "inherit";
                                jQuery('#login_error').append('<p>' + error + '</p>');
                            }
                            //
                        });
                        $('.img-profile').initial();
                        if (response.response_status == 400) {
                            $('#Categorization_info').append('');
                        } else {

                            var $i = 1;
                            $.each(response, function (key, value) {
                                $("#Accreditation_info").append("" +
                                    "<tr><td class='text-center'>" + $i++ +"</td>" +
                                    '<td>' + this.client_name + '</td>' +
                                    '<td>' + this.client_email + '</td>' +
                                    '<td>' + this.client_phone + '</td>' +
                                    '<td>' + this.client_website + '</td>' +
                                    '<td>' + this.country_name + '</td>' +
                                    // '<td>' + this.client_city + '</td>' +
                                    // '<td>' + this.client_state + '</td>' +
                                    // '<td>' + this.client_address + '</td>' +
                                    // '<td>' + this.client_notes + '</td>' +
                                    '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                                    '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                                    '</tr>');
                            });
                            /*
                            |--------------------------------------------
                            | Load Data Table
                            |--------------------------------------------
                            */
                            var table = $('#provincesTable').DataTable();
                        }

                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                    //
                });
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
//
        });
    });
    $(document).on('click', '.edit-modal', function() {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: '../api/auth/ShowClientsById/' + id + '',
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {

                JSON.stringify(response); //to string
                $.each(response, function(index, value) {
                    $('#client_country_edit').append('' +
                        '<option value=' + this.id + ' selected>' + value.country_name + '</option>');

                });

                $('.form-horizontal').show();
                $('#id_edit').val(response[0].id);
                $('#client_name_edit').val(response[0].client_name);
                $('#client_email_edit').val(response[0].client_email);
                $('#client_phone_edit').val(response[0].client_phone);
                $('#client_website_edit').val(response[0].client_website);
                $('#client_city_edit').val(response[0].client_city);
                $('#client_state_edit').val(response[0].client_state);
                $('#client_address_edit').val(response[0].client_address);
                $('#client_notes_edit').val(response[0].client_notes);

            },
            error: function(xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });
    });
    $(document).on('click', '#update_clients', function() {
        $('#update_clients').html('Updating..');
        var formData = $("#EditClients").serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/UpdateClient",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(response) {
                JSON.stringify(response);
                console.log(response.response_message);
                jQuery('#updating_error').show();
                document.getElementById("updating_error_edit").style.display = "inherit";
                jQuery('#updating_error_edit').append('<p>' + response.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);

            },
            error: function(xhr, status, error) {
              console.log(xhr.responseText);
            }
        });

    });
    $(document).on('click', '.delete', function() {
        var id = $(this).attr("id");
//            alert(id);
        if (confirm("Are you sure you want to delete this records?")) {
            $.ajax({
                type: 'post',
                url: "../api/auth/DeleteClient",
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                },
                data: {
                    id: id
                },
                success: function(response) {
                    JSON.stringify(response); //to string
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                    setTimeout(function() {
                        window.location.reload()
                    }, 2000);

                },
                error: function(xhr, status, error) {
//                        jQuery('#login_error').show();
//                        document.getElementById("login_error").style.display = "inherit";
//                        jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                    console.log(xhr.responseText);
                }
            });
        }
    });
}
