@extends('backend.layout.master')

@section('title', 'Accreditx Hospital Info')

@section('content')
    {{--<link href="../formwizard/css/bootstrap.min.css" rel="stylesheet" />--}}

    <script src="../backend/js/sidemenu.js" defer></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link href="../formwizard/css/demo.css" rel="stylesheet" />
    <link href="../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        /*.stepperUI .panel:first-child{*/
        /*display: block;*/
        /*}*/
        /*.moving-tab{*/
        /*width: auto !important;*/
        /*}*/
        .form-control, .form-group .form-control {
            border: 0;
            background-image: linear-gradient(#9c27b0, #9c27b0), linear-gradient(#D2D2D2, #D2D2D2);
            background-size: 0 2px, 100% 1px;
            background-repeat: no-repeat;
            background-position: center bottom, center calc(100% - 1px);
            background-color: transparent;
            transition: background 0s ease-out;
            float: none;
            box-shadow: none;
            border-radius: 0;
            font-weight: 400;
            text-align: right;
            font-size: 17px;
        }
        .list_findings li{
            list-style: none;
            padding: 10px;
        }
        .wizard-header{
            /*margin: 5px 0 0;*/
            text-align: left;
            border-top: 1px solid #00245c;
            border-bottom: 1px solid #00245c;
            padding: 0.5em;
            font-size: 16px;
            font-weight: bold;
            background: #f3f3f3;
            color: #00245c;
        }
        .wizard-title{
            text-align: left;
        }
        .wizard-card .wizard-header {
            /*text-align: center;*/
            padding: 10px !important;
        }
        .level_parent{
            display: flex;
            width: 100%;
            /*margin: 5px 0 0;*/
            text-align: left;
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 3.5em;
            font-size: 16px;
            font-weight: lighter;
            /*background: #f3f3f3;*/
        }
        @keyframes check {0% {height: 0;width: 0;}
            25% {height: 0;width: 10px;}
            50% {height: 20px;width: 10px;}
        }
        .checkbox{background-color:#fff;display:inline-block;height:28px;margin:0 .25em;width:28px;border-radius:4px;border:1px solid #ccc;float:right}
        .checkbox span{display:block;height:28px;position:relative;width:28px;padding:0}
        .checkbox span:after{-moz-transform:scaleX(-1) rotate(135deg);-ms-transform:scaleX(-1) rotate(135deg);-webkit-transform:scaleX(-1) rotate(135deg);transform:scaleX(-1) rotate(135deg);-moz-transform-origin:left top;-ms-transform-origin:left top;-webkit-transform-origin:left top;transform-origin:left top;border-right:4px solid #fff;border-top:4px solid #fff;content:'';display:block;height:20px;left:3px;position:absolute;top:15px;width:10px}
        .checkbox span:hover:after{border-color:#999}
        .checkbox input{display:none}
        .checkbox input:checked + span:after{-webkit-animation:check .8s;-moz-animation:check .8s;-o-animation:check .8s;animation:check .8s;border-color:#555}
        .checkbox input:checked + .default:after{border-color:#444}
        .checkbox input:checked + .primary:after{border-color:#2196F3}
        .checkbox input:checked + .success:after{border-color:#8bc34a}
        .checkbox input:checked + .info:after{border-color:#3de0f5}
        .checkbox input:checked + .warning:after{border-color:#FFC107}
        .checkbox input:checked + .danger:after{border-color:#f44336}

        .wizard > .content {
            min-height: 100px !important;
            width: auto;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            width: 24px;
            height: 24px;
            line-height: 26px;
            display: block;
            font-size: 12px;
            color: #333;
            background: white;
            border-radius: 25px;
            margin: 0 auto 10px auto;
        }

        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #4CAF50;
        }
        .stepwizard-step p {
            margin-top: 0px;
            color:#666;
        }
        .stepwizard-row {
            display: table-row;
        }
        .stepwizard {
            display: table;
            width: 100%;
            position: relative;
        }
        .stepwizard-step button[disabled] {
            /*opacity: 1 !important;
            filter: alpha(opacity=100) !important;*/
        }
        .stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
            opacity:1 !important;
            color:#bbb;
        }
        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content:" ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
            z-index: 0;
        }
        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }
        .btn-circle {
            width:30px;
            height: 30px;
            line-height: 26px;
            display: block;
            font-size: 14px;
            background: #36b9cc;
            color: white;
            border-radius: 50%;
            margin: 0 auto 10px auto;
        }
        .nav-pills > li > a{
            outline: none;
            border: 0 !important;
            border-radius: 0;
            line-height: 18px;
            text-transform: uppercase;
            font-size: 12px;
            font-weight: bold;
            min-width: 100px;
            text-align: center;
            color: #fff !important;
            text-decoration: none;
            background: #36b9cc !important;
            padding: 10px;
        }
        .wrapper{
            width:100%;
        }
        /*@media(max-width:992px){*/
        /*.wrapper{*/
        /*width:100%;*/
        /*}*/
        /*}*/
        .panel-heading {
            padding: 0;
            border:0;
        }
        .card {
            border: none !important;
            border-radius: 0px !important;
        }
        .panel-title{
            display:flex;
            padding:15px;
            color:#555;
            font-size:13px;
            font-weight:bold;
            text-transform:uppercase;
            letter-spacing:1px;
            word-spacing:3px;
            text-decoration:none;
            /*background: #d5ecff;*/
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
        }
        .panel-title a{
            color:#555;
            font-size:13px;
            font-weight:bold;
            text-transform:uppercase;
            letter-spacing:1px;
            word-spacing:3px;
            text-decoration:none;

        }
        .panel-title p{
            color:#555;
            font-size:13px;
            font-weight:bold;
            text-transform:uppercase;
            letter-spacing:1px;
            word-spacing:3px;
            text-decoration:none;
            text-align: right;
        }
        .panel-title>a, .panel-title>a:active{
            display:block;
            padding:15px;
            color:#555;
            font-size:13px;
            font-weight:bold;
            text-transform:uppercase;
            letter-spacing:1px;
            word-spacing:3px;
            text-decoration:none;
            box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
        }
        .panel-body{
            background-color: rgba(221, 221, 221, 0.3);
            padding: 15px;
            margin-bottom: 5px;

        }
        .modal{
            height: auto !important;
        }
        .nav.nav-tabs {
            float: left;
            display: block;
            /*margin-right: 20px;*/
            border-bottom:0;
            border-right: 1px solid #ddd;
            /*padding-right: 15px;*/
        }
        .nav-tabs .nav-link {
            border-radius:0px !important;
            display:block;
            padding:15px;
            color:#00235d;
            font-size:11px;
            font-weight:bold;
            text-transform:uppercase;
            letter-spacing:1px;
            word-spacing:3px;
            text-decoration:none;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .nav-tabs .nav-link.active {
            color: #fff;
            background-color: #36b9cc !important;
            border-color: transparent !important;
        }
        /*.nav-tabs .nav-link {*/
        /*border: 1px solid transparent;*/
        /*border-top-left-radius: 0rem!important;*/
        /*border-top-right-radius: 0rem!important;*/
        /*}*/
        .tab-content>.active {
            display: block;
            /*background: #007bff;*/
            min-height: 165px;
        }
        .modal-content {
            border-radius: 0px;
            border: none !important;
        }
    </style>
    <!-- Page Wrapper -->

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>
                <div class='container-fluid'>
                    {{--<ul class="nav nav-tabs" role="tablist">--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#home" role="tab" aria-controls="home">Home</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">Profile</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-controls="messages">Messages</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-controls="settings">Settings</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--<div class="tab-content">--}}
                    {{--<div class="tab-pane active" id="home" role="tabpanel">1</div>--}}
                    {{--<div class="tab-pane" id="profile" role="tabpanel">..2.</div>--}}
                    {{--<div class="tab-pane" id="messages" role="tabpanel">.3..</div>--}}
                    {{--<div class="tab-pane" id="settings" role="tabpanel">.4..</div>--}}
                    {{--</div>--}}
                    <div class="row" style="margin-bottom: 15px">
                        <div class="col-lg-12">
                            <div class="wizard-container">
                                <div class="card wizard-card" data-color="green" id="wizardProfile">
                                    {{--<div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>--}}
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <ul class="nav nav-tabs" id="assessment_navigation" role="tablist">
                                            </ul>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="tab-content">

                                                <div class="row h-100">
                                                    <div class="modal fade" id="successmessage" role="dialog">
                                                        <div class="modal-dialog  my-auto">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                                                </div>

                                                                <div class="modal-body" id="successmodal">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- wizard container -->
                        </div>

                    </div>

                </div> <!--  big container -->

            </div>

            @include('backend.layout.footer')
        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="#">Logout</a>
                </div>
            </div>
        </div>
    </div>


    </div>

    <!--   Core JS Files   -->
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script type="application/javascript">
        var data  = sessionStorage.getItem('accessToken');
        if(!data == data){
            window.location.href = "/";
        }else{
            $(document).ready(function () {
                $.ajax({
                    type: 'get',
                    url: "../api/auth/RiskAreaWithStandards",
                    dataType: 'json',
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response);
                        $.each(response, function (index, value) {
//                        console.log('risk area',value.riskArea_name);

                            $('#assessment_navigation').append('' +
                                '<li class="nav-item loop_tabs"><a class="nav-link" data-toggle="tab" href="#name'+ value.id +'" role="tab" aria-controls="name'+ value.id +'">'+ value.riskArea_name +'</a> </li>');
                            $('.tab-content').append('' +'<div class="tab-pane" id="name'+ value.id +'" role="tabpanel"><div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"><div class="panel panel-default" id="accordions_tabs'+ value.id +'"></div>');
                            $.each(value.standards, function(index, standard){
                                $('#accordions_tabs'+ value.id +'').append('' +'<div class="panel-heading" role="tab" id="heading'+ standard.id +'">' +

                                    '<h4 class="panel-title">'+
                                    '<div class="col-lg-9">'+
                                    '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+ standard.id +'" aria-expanded="true" aria-controls="collapse'+ standard.id +'">'+ standard.standards_name +'</a>'+
                                    '</div>'+
                                    '<div class="col-lg-3">'+
                                    '<p>Overall</a>'+
                                    '</div>'+
                                    '</h4>'+
                                    '</div>'+
                                    '<div id="collapse'+ standard.id +'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+ standard.id +'">'+
                                    '<input type="text" name="user_id" id="user_id" hidden><input type="text" name="hospital_id" id="hospital_id" hidden>'+
                                    '</div>');

                                $.each(standard.levels, function(index, levels){
                                    $('#collapse'+ standard.id +'').append('' + '<div class="panel-body" style="display: flex;width: 100%;" data-id=' + levels.id + ' id=' + levels.id + '><div class=\'col-lg-3 level_loops\'><p>Level '+levels.order_number+'.&nbsp&nbsp&nbsp'+levels.levels_name+'</p><div class="form-group"> <textarea rows="5" class="form-control form-control-user" name="assessment_comment" id="assessment_comment'+ levels.id +'" placeholder="Assessment Comment" style="text-align: left !important;color: #000 !important;font-size: 16px !important;display: none" required></textarea> </div></div><div class="col-lg-7 perform_'+levels.id+'"><ul class="list_findings_'+ levels.id +'"></ul></div><div class="col-lg-2"><div class="form-group"> <input type="text" class="form-control form-control-user" name="assessment_mark" id="assessment_mark'+ levels.id +'" readonly required><input type="text" name="perfom_finding_id" id="perfom_finding_id'+levels.id+'" hidden></div><div class="alert alert-success text-center" id="login_error'+ levels.id +'" style="margin-top: 10px; width: 100%; display: none; position: absolute;"></div></div></div>');

                                    $.each(levels.perform_finding, function(index, perform_finding){
                                        $('.list_findings_'+ levels.id +'').append('' + '<li style="list-style: none;"><span>'+perform_finding.order_number+'&nbsp&nbsp&nbsp'+perform_finding.perfom_findings_name+'<label class="checkbox"><input type="checkbox" name="type[]" class="findings_'+levels.id+'" data-levelsid="'+levels.id+'"  data-name="'+levels.order_number+'" data-finding_id="'+perform_finding.id+'" data-id="'+perform_finding.order_number+'" id="findings_'+perform_finding.id+'" required/><span class="primary"></span></label></span></li>');
                                    });
//                                    $(document).ready(function () {
//                                        var id = $(this).attr("data-levelperform");
//                                        $.ajax({
//                                            type: 'post',
//                                            url: '../api/auth/CheckAssessment/' + id +'',
//                                            dataType: 'json',
//                                            beforeSend : function( xhr ) {
//                                                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
//                                            },
//                                            data:{id:id},
//                                            success: function (response,xhr) {
//                                                JSON.stringify(response); //to string
////                                                    jQuery('#login_error').show();
////                                                    document.getElementById("login_error").style.display = "inherit";
////                                                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
////                                                    console.log(xhr.responseText);
//
//                                            }, error: function (xhr, status, error) {
//                                                console.log(xhr.responseText);
//                                            }
//                                        });
////
//                                    });
                                    $("[data-id='"+levels.id+"'] input:checkbox").click(function(){
                                        var $inputs = $("[data-id='"+levels.id+"'] input:checkbox");
                                        var id = $(this).attr("data-id");
                                        var name = $(this).attr("data-name");
                                        var finding_id = $(this).attr("data-finding_id");
                                        var levelsid = $(this).attr("data-levelsid");
                                        console.log(levelsid);
                                        var AssessmentLevel = (id * name);


                                    });
                                    if($(this).is(':checked')){
//                                        console.log('something',AssessmentLevel);
                                        $('#assessment_mark'+ levels.id +'').val(AssessmentLevel);
                                        $('#assessment_comment'+ levels.id +'').show();
                                        $('#perfom_finding_id'+ levels.id +'').val(finding_id);
                                        $inputs.not(this).prop('disabled',true); // <-- disable all but checked one

                                        var hospital_id =$('#hospital_id').val();
                                        var user_id =$('#user_id').val();
                                        var assessment_mark =$('#assessment_mark'+ levels.id +'').val();
                                        var perfom_finding_id =$('#perfom_finding_id'+levels.id+'').val();

                                        $.ajax({
                                            type: 'post',
                                            url: "../api/auth/AddAssessment",
                                            dataType: 'json',
                                            beforeSend : function( xhr ) {
                                                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                                            },
                                            data: {
                                                'hospital_id': hospital_id,
                                                'user_id': user_id,
                                                'assessment_mark': assessment_mark,
                                                'perfom_finding_id': perfom_finding_id,
                                                'levelsid': levelsid,
                                            },
                                            success: function(data, xhr, response) {
                                                JSON.stringify(response); //to string
                                                $('#successmodal').html('<p style="text-align:center;">' + data.response_message + '</p>');
                                                $('#successmessage').modal('show');
//
                                            },
                                            error: function(xhr, status, error) {
                                                console.log(xhr.responseText);
                                            }
                                        });

                                    }else{
                                        $('#assessment_comment'+ levels.id +'').hide();
                                        $('#login_error'+ levels.id +'').hide();
                                        $('#assessment_mark'+ levels.id +'').val("");
                                        $('#perfom_finding_id'+ levels.id +'').val("");
                                        $inputs.prop('disabled',false); // <--
                                    }
                                });
                                $("[data-id='"+standard.id+"'] input:checkbox").click(function(){
                                    if($(this).is(':checked')){
                                        $("#SaveAssess").show();
                                    }else{
                                        $("#SaveAssess").hide();
                                    }
                                });

                                $(document).ready(function () {
                                    $.ajax({
                                        type: 'post',
                                        url: "../api/auth/me",
                                        dataType: 'json',
                                        beforeSend : function( xhr ) {
                                            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                                        },
                                        success: function (response) {
                                            JSON.stringify(response); //to string
                                            $('#user_id').val(response.id);
                                        }, error: function (xhr, status, error) {
                                            console.log(xhr.responseText);
                                        }
                                    });
                                });

                                var url_string = location.href;
                                var urlink = new URL(url_string);
                                var id = urlink.searchParams.get("id");
                                $('#hospital_id').val(id);
                            });
                        });

                    });
            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
        });

        });

        $(document).ready(function () {
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();

            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                var $target = $(e.target);

                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });

            $(".next-step").click(function (e) {

                var $active = $('.nav-tabs li>a.active');
                $active.parent().next().removeClass('disabled');
                nextTab($active);

            });
            $(".prev-step").click(function (e) {

                var $active = $('.nav-tabs li>a.active');
                prevTab($active);

            });
        });

        function nextTab(elem) {
            $(elem).parent().next().find('a[data-toggle="tab"]').click();
        }
        function prevTab(elem) {
            $(elem).parent().prev().find('a[data-toggle="tab"]').click();
        }

        }

    </script>
    <script>
        $('.panel-collapse').on('show.bs.collapse', function () {
            $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function () {
            $(this).siblings('.panel-heading').removeClass('active');
        });
    </script>
    <script src="../formwizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    {{--<script src="../formwizard/js/bootstrap.min.js" type="text/javascript"></script>--}}
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="../formwizard/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="../formwizard/js/jquery.validate.min.js"></script>

    {{--<script src="../backend/vendor/jquery/jquery.min.js"></script>--}}
    <script src="../backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="../backend/js/custom.js"></script>
    <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="../js/demo/datatables-demo.js"></script>

@endsection



<div class="row  multi-field">
    <div class='col-lg-12'>
        <div class="form-group">
            <textarea id="summernote" name="perfom_findings_name"></textarea>
        </div>
    </div>
    <div class='col-lg-12'>

        <div class="form-group">
            <input type="text" class="form-control form-control-user" name="order_number_finding[]" id="order_number_finding"  placeholder="Order Number" required>
        </div>
    </div>
    <button type="button" class="remove-field btn btn-danger btn-circle" style="margin-left: 10px;"><i class="fas fa-trash"></i> </button>
</div>