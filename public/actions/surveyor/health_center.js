var data  = sessionStorage.getItem('accessToken');
if(!data == data){
    window.location.href = "/";
}else{

    $(document).ready(function () {
        $.ajax({
            type: 'get',
            url: "../api/auth/Provinces",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $.each(response, function (index, value) {
                    $('.province_id').append('' +
                        '<option value=' + this.id +'>'+ this.name +'</option>');

                });

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
//
        });
    });
    $(document).on('change', '.province_id', function() {

        var District_id =$('.province_id').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/auth/Districts",
            data: {
                'province_id': District_id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $.each(response, function (index, value) {
                    $('.district_id').append('' +
                        '<option value=' + this.id +'>'+ this.district_name +'</option>');
                });

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
        });
    });
    $(document).on('change', '.district_id', function() {

        var id =$('.district_id').val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/auth/PopulationCovered",
            data: {
                'id': id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $('.district_population').val(response[0].district_population);

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
        });
    });
    $(document).ready(function () {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowAddHospitalCategory",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
//                    console.log(response);
                $.each(response, function (index, value) {
                    $('.hospital_cat_id').append('' +
                        '<option value="'+ this.hospital_category_name +'">'+ this.hospital_category_name +'</option>');
                });
//                       alert(response.access_token);

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
//
        });
//            setInterval(request, 50000);
    });
    $(document).on('click', '#save_Health_info', function() {
        var formData = $("#addModalForm").serialize();
//        alert(JSON.stringify(formData));
        $.ajax({
            type: "POST",
            url: "../api/auth/AddHospitalInfo",
            data: formData,
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function (response) {
                JSON.stringify(response); //to string
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+response.response_message+'</p>');
                setTimeout(function() { window.location.reload() }, 2000);

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+response.response_message+'</p>');
//                  console.log(xhr.responseText);
            }
        });

    });
    $(document).ready(function () {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowHealthCenter",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                console.log(response);
                if(response.response_status == 400){
                    $('#Hospital_Info').append('');
                }else {
                    var $i = 1;
                    $.each(response, function (key, value) {
                        $("#Hospital_Info").append("" +
                            "<tr><td class='text-center'>" + $i++ +"</td>" +
                            '<td>' + this.name + '</td>' +
                            '<td>' + this.district_name + '</td>' +
                            '<td>' + this.facility_name + '</td>' +
                            '<td>' + this.hospital_cat_id + '</td>' +
                            '<td>' + this.name_of_facility_head + '</td>' +
                            '<td>' + this.phone_of_head_facility + '</td>' +
                            '<td><a href="/AccreditxSurvey?id=' + this.id + '" class="btn btn-warning btn-circle action_btn"> <i class="fas fa-vote-yea"></i> </a></td>' +
                            '<td><a href="/ShowHospitalInfoMore?id=' + this.id + '" class="btn btn-info btn-circle action_btn"> <i class="fas fa-eye"></i> </a></td>' +
                            '</tr>');
                    });
                    /*
                    |--------------------------------------------
                    | Load Data Table
                    |--------------------------------------------
                    */
                    var table = $('#provincesTable').DataTable();
                }

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
//
        });
//            setInterval(request, 50000);
    });
    $(document).on('click', '.delete', function(){
        var id = $(this).attr("id");
        if(confirm("Are you sure you want to delete this records?"))
        {
            $.ajax({
                type: 'post',
                url: "../api/auth/DeleteHospitalInfo",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                data:{id:id},
                success: function (response) {
                    JSON.stringify(response); //to string
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                    setTimeout(function() { window.location.reload() }, 3000);

                }, error: function (xhr, status, error) {
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
//                    console.log(xhr.responseText);
                }
            });
        }
    });
    $(document).on('click', '.edit-modal', function() {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: '../api/auth/ShowEditHealthCenter/' + id +'',
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {

                JSON.stringify(response); //to string
//                    console.log(response);
                $.each(response, function (index, value) {
                    $('#province_id_edit').append('' +
                        '<option value="'+ this.province_id +'" selected>'+ this.name +'</option>');
                });
                $.each(response, function (index, value) {
                    $('#district_id_edit').append('' +
                        '<option value="'+ this.district_id +'" selected>'+ this.district_name +'</option>');
                });
                $.each(response, function (index, value) {
                    $('#hospital_cat_id_edit').append('' +
                        '<option value="'+ this.hospital_cat_id +'" selected>'+ this.hospital_cat_id +'</option>');
                });
                $('.form-horizontal').show();
                $('#id_edit').val(response[0].id );
                $('#facility_name_edit').val(response[0].facility_name);
                $('#facility_phone_edit').val(response[0].facility_phone);
                $('#facility_email_edit').val(response[0].facility_email);
                $('#name_of_facility_head_edit').val(response[0].name_of_facility_head);
                $('#phone_of_head_facility_edit').val(response[0].phone_of_head_facility);
                $('#name_alternative_facility_head_edit').val(response[0].name_alternative_facility_head);
                $('#phone_alternative_facility_head_edit').val(response[0].phone_alternative_facility_head);
                $('#population_covered_edit').val(response[0].population_covered);
                $('#total_beds_edit').val(response[0].total_beds);
                $('#maternity_beds_edit').val(response[0].maternity_beds);
                $('#operational_ambulances_edit').val(response[0].operational_ambulances);
                $('#general_practitioner_edit').val(response[0].general_practitioner);
                $('#specialists_edit').val(response[0].specialists);
                $('#a1_edit').val(response[0].a1);
                $('#a2_edit').val(response[0].a2);
                $('#mid_wives_edit').val(response[0].mid_wives);
                $('#lab_techs_edit').val(response[0].lab_techs);
                $('#physio_edit').val(response[0].physio);
                $('#anesthetists_edit').val(response[0].anesthetists);
                $('#pharmacists_edit').val(response[0].pharmacists);
                $('#dentists_edit').val(response[0].dentists);

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + response.response_message + '</p>');
            }
        });
    });
    $(document).on('click', '#update_category', function () {
        $('#update_category').html('Updating..');
        var formData = $("#editModalForm").serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/UpdateHospitalInfo",
            data: formData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function (response) {
                JSON.stringify(response); //to string
                console.log(response);
                jQuery('#updating_error').show();
                document.getElementById("updating_error").style.display = "inherit";
                jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                setTimeout(function() { window.location.reload() }, 2000);

            }, error: function (xhr, status, error) {
//                    jQuery('#updating_error').show();
//                    document.getElementById("updating_error").style.display = "inherit";
//                    jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });

    });
}
