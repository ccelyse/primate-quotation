@extends('backend.layout.master')

@section('title', 'Primate')

@section('content')
    {{--<script src="../../backend/js/sidemenu.js"></script>--}}
    <style>
        .card-body {
            flex: 1 1 auto;
            padding: 10px;
        }
        #addModalForm{
            width: 100%;
            padding-bottom: 15px;
            background: #fff;
            margin-bottom: 5px;
            padding: 10px;
        }
        .highcharts-title{
            font-family: Circular !important;
            font-size: 14px !important;
        }
        .highcharts-axis-labels{
            font-family: Circular !important;
            font-size: 12px !important;
        }
        .highcharts-color-0{
            fill: #4e80bd !important;
        }
        .highcharts-color-2{
            fill: #9bbb59 !important;
        }
        #filter_report {
            float: right;
            margin-top: 20px;
        }
        .action_btn{
            margin-bottom: 10px;
        }
        #action_btn{
            display: block;
        }
    </style>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>

        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>
                <div class='container-fluid'>
                    <button class="btn btn-success action_btn d-print-none" id="action_btn" onclick="myFunction()">Print this Quotation</button>
                        <div class="card shadow">
                            <div class="card-body" style="    display: inline-flex;">

                        <div class="col-md-6">
                            <img src="backend/img/logo.png" style="width: 150px;">
                            <br>Gasabo, Nyarutarama,
                            <br>KG 9 Av, KG270 St,
                            <br>PO Box 4158,
                            <br>Rwanda
                            <br>Tel: +250 789 734 384
                            <br>Email: info@primatesafaris.info
                        </div>
                        <div class="col-md-6" style="text-align: right">
                            <span id="agent_name"></span>
                            <br><span id="agent_country_city"></span>
                            <br><span id="agent_address"></span>
                            <br><span id="agent_phone"></span>
                            <br><span id="agent_email"></span>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;padding-bottom: 50px;">
                            <div class="card shadow">
                                <div class="card-body">
                                    <table class="table table-bordered border- text-black">
                                        <thead>
                                        <tr class="tableizer-firstrow">
                                            <th></th>
                                            <th></th>
                                            <th colspan="3" >STO RATES</th>
{{--                                            <th>&nbsp;</th>--}}
{{--                                            <th>&nbsp;</th>--}}
{{--                                            <th>&nbsp;</th>--}}
                                            <th colspan="3" >RACK RATES</th>
{{--                                            <th>&nbsp;</th>--}}
{{--                                            <th>&nbsp;</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody id="loop_table_level">
                                        <tr>
                                            <td>Date</td>
                                            <td>Description</td>
                                            <td>Unit Cost</td>
                                            <td>Units </td>
                                            <td>Total Cost</td>
                                            <td>Unit Cost</td>
                                            <td>Units </td>
                                            <td>Total Cost</td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<td>13-Mar-20</td>--}}
                                            {{--<td>Arrival Transfer Per Vehicle</td>--}}
                                            {{--<td> $60.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $60.00 </td>--}}
                                            {{--<td>&nbsp;</td>--}}
                                            {{--<td> $80.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $80.00 </td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>&nbsp;</td>--}}
                                            {{--<td>VIP Meet and Greet Per Person </td>--}}
                                            {{--<td> $25.00 </td>--}}
                                            {{--<td>3</td>--}}
                                            {{--<td> $75.00 </td>--}}
{{--                                            <td>&nbsp;</td>--}}
                                            {{--<td> $30.00 </td>--}}
                                            {{--<td>3</td>--}}
                                            {{--<td> $90.00 </td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>&nbsp;</td>--}}
                                            {{--<td>Accommodation at Kigali Serena Hotel Double Per Night (Prime Room)</td>--}}
                                            {{--<td> $420.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $420.00 </td>--}}
{{--                                            <td>&nbsp;</td>--}}
                                            {{--<td> $495.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $495.00 </td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>&nbsp;</td>--}}
                                            {{--<td>Accommodation at Kigali Serena Hotel Single Per Night (Deluxe Room)</td>--}}
                                            {{--<td> $365.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $365.00 </td>--}}
{{--                                            <td>&nbsp;</td>--}}
                                            {{--<td> $430.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $430.00 </td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>14-Mar-20</td>--}}
                                            {{--<td>Transport Per Vehicle Per Day</td>--}}
                                            {{--<td> $345.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $345.00 </td>--}}
{{--                                            <td>&nbsp;</td>--}}
                                            {{--<td> $360.00 </td>--}}
                                            {{--<td>1</td>--}}
                                            {{--<td> $360.00 </td>--}}
                                        {{--</tr>--}}


                                        </tbody>
                                        <tbody id="loop_table_total">
                                        {{--<tr>--}}
                                            {{--<td>&nbsp;</td>--}}
                                            {{--<td>Total</td>--}}
                                            {{--<td> $1,953.33 </td>--}}
                                            {{--<td> $5,860.00 </td>--}}
                                            {{--<td> $2,085.00 </td>--}}
                                            {{--<td> $6,255.00</td>--}}
                                            {{--<td> $6,255.00</td>--}}
                                            {{--<td> $6,255.00</td>--}}
                                        {{--</tr>--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="../js/jquery.js" type="application/javascript"></script>
    <script>
        function myFunction() {
            window.print();
            // document.getElementById("action_btn").style.display = "none";
        }
    </script>
    <script src="../../backend/js/custom.js"></script>
    <script src="../../js/initial.min.js"></script>
    <script src="../../actions/systemPdf.js"></script>
