@extends('backend.layout.master')
@section('title', 'Primate Safaris')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>

                <div class='container-fluid'>
                    <div class="row" style="margin-bottom: 15px">
                        <div>
                            <button type="button" class="btn btn-success action_btn edit-modal" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important;">
                                <i class="fas fa-plus-circle"></i> Add new
                            </button>
                        </div>
                        <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="max-width: 60% !important;">
                                <div class="modal-content">
                                    <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                    <div class="modal-header">
                                        {{--
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                                <form action="#" id="AddAgents" enctype="multipart/form-data">
                                                    <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                                    <div class="tab-content">
                                                        <div class="row">
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="agent_name" id="agent_name" placeholder="Name" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="email" class="form-control form-control-user" name="agent_email" id="agent_email" placeholder="Email" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="number" class="form-control form-control-user" name="agent_phone" id="agent_phone" placeholder="Phone Number" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user" name="agent_country" id="agent_country" required>
                                                                        <option value="">Select Country</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="agent_city" id="agent_city" placeholder="City" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="agent_state" placeholder="State" id="agent_state" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="agent_address" placeholder="Address" id="agent_address" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                                <div class="form-group">
                                                                    <textarea rows="5" class="form-control form-control-user" name="agent_notes" id="agent_notes" placeholder="Note" required></textarea>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6' hidden>
                                                                <div class="form-group" >
                                                                    <input type="text" class="form-control form-control-user" name="agent_number" id="agent_number" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6' hidden>
                                                                <div class="form-group" >
                                                                    <input type="text" class="form-control form-control-user" name="user_id" id="id_hosp" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <input type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="add_agents" style="border-radius:0px !important;"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                        </div>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Clients</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="provincesTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Percentage</th>
                                            <th>Country</th>
{{--                                            <th>City</th>--}}
{{--                                            <th>State</th>--}}
{{--                                            <th>Address</th>--}}
{{--                                            <th>Note</th>--}}
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody id="Accreditation_info">

                                        </tbody>
                                    </table>

                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="alert alert-success text-center" id="updating_error_edit" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Agent</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="#" id="EditAgents" enctype="multipart/form-data">
                                                        <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                                        <div class="tab-content">
                                                            <div class="row">
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control form-control-user" name="agent_name" id="agent_name_edit" placeholder="Name" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="email" class="form-control form-control-user" name="agent_email" id="agent_email_edit" placeholder="Email" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="number" class="form-control form-control-user" name="agent_phone" id="agent_phone_edit" placeholder="Phone Number" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="url" class="form-control form-control-user" name="agent_percentage" id="agent_percentage_edit" placeholder="Percentage" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <select class="form-control form-control-user" name="agent_country" id="agent_country_edit" required>
{{--                                                                            <option value="">Select Country</option>--}}
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="agent_city" id="agent_city_edit" placeholder="City" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="agent_state" placeholder="State" id="agent_state_edit" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="agent_address" placeholder="Address" id="agent_address_edit" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-12'>
                                                                    <div class="form-group">
                                                                        <textarea rows="5" class="form-control form-control-user" name="agent_notes" id="agent_notes_edit" placeholder="Note" required></textarea>
                                                                    </div>
                                                                </div>

                                                                <div class='col-lg-6' hidden>
                                                                    <div class="form-group" >
                                                                        <input type="text" class="form-control form-control-user" name="user_id" id="user_edit" required>
                                                                        <input type="text" class="form-control form-control-user" name="id_edit" id="id_edit" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <input type='button' class='btn btn-fill btn-success btn-wd' value='Update' id="update_agents" style="border-radius:0px !important;"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
            </div>
            <!-- end row -->

        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="../js/demo/datatables-demo.js"></script>
    <script src="../../actions/systemAgents.js"></script>
@endsection
