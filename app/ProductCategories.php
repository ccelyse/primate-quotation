<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = "product_categories";
    protected $fillable = ['id','product_categories_name'];
}
