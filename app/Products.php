<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    protected $fillable = ['id','product_category_id','provider_id','product_price','provider_contract_id'];
}
