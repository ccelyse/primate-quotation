<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('CreateAccount', 'BackendController@CreateAccount');

Route::group([

//    'middleware' => 'api',
    'prefix' => 'auth'

], function () {

    Route::post('login', 'AuthController@login');
    Route::post('SignUp', 'AuthController@SignUp');
    Route::post('DeleteAccount', 'AuthController@DeleteAccount');
    Route::get('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::get('UserInfo/{id}', 'AuthController@UserInfo');
    Route::get('Roles', 'BackendController@Roles');
    Route::get('ShowRoles', 'BackendController@ShowRoles');
    Route::get('ShowSystemAccount', 'BackendController@ShowSystemAccount');
    Route::post('ShowUserAccount', 'BackendController@ShowUserAccount');
    Route::post('UpdateUserAccount', 'BackendController@UpdateUserAccount');
    Route::get('DashboardCount', 'BackendController@DashboardCount');



    /*BACKEND API ROUTES */

    Route::post('AddClients', 'BackendController@AddClients');
    Route::get('ShowClients', 'BackendController@ShowClients');
    Route::get('Countries', 'BackendController@Countries');
    Route::get('ShowClientsById/{id}', 'BackendController@ShowClientsById');
    Route::post('UpdateClient', 'BackendController@UpdateClient');
    Route::post('DeleteClient', 'BackendController@DeleteClient');

    Route::post('AddProductCategory', 'BackendController@AddProductCategory');
    Route::get('ShowProductCategory', 'BackendController@ShowProductCategory');
    Route::get('ShowProductCategoryById/{id}', 'BackendController@ShowProductCategoryById');
    Route::post('UpdateProductCategory', 'BackendController@UpdateProductCategory');
    Route::post('DeleteProductCategory', 'BackendController@DeleteProductCategory');

    Route::post('AddProductProvider', 'BackendController@AddProductProvider');
    Route::get('ShowProductProvider', 'BackendController@ShowProductProvider');
    Route::get('ShowProductProviderById/{id}', 'BackendController@ShowProductProviderById');
    Route::post('UpdateProductProvider', 'BackendController@UpdateProductProvider');
    Route::post('DeleteProductProvider', 'BackendController@DeleteProductProvider');
    Route::post('ShowProviderByCategory', 'BackendController@ShowProviderByCategory');
    Route::post('ShowContractByProvider', 'BackendController@ShowContractByProvider');

    Route::post('AddContract', 'BackendController@AddContract');
    Route::get('ShowContract', 'BackendController@ShowContract');
    Route::post('ShowContractPrice', 'BackendController@ShowContractPrice');

    Route::post('AddQuotation', 'BackendController@AddQuotation');
    Route::get('ShowQuotation', 'BackendController@ShowQuotation');
    Route::post('ShowQuotationDetails', 'BackendController@ShowQuotationDetails');
    Route::post('ShowQuotationAgentId', 'BackendController@ShowQuotationAgentId');
    Route::post('AddQuotationDays', 'BackendController@AddQuotationDays');
    Route::post('ShowQuotationDays', 'BackendController@ShowQuotationDays');
    Route::post('ShowQuotationDaysAgentId', 'BackendController@ShowQuotationDaysAgentId');
    Route::post('AddMoreQuotationDays', 'BackendController@AddMoreQuotationDays');
    Route::post('DeleteMoreQuotationDays', 'BackendController@DeleteMoreQuotationDays');
    Route::post('GeneratePdf', 'BackendController@GeneratePdf');
    Route::post('GeneratePdfClient/', 'BackendController@GeneratePdfClient');
    Route::post('DeleteQuotationDays/', 'BackendController@DeleteQuotationDays');
    Route::post('DeleteQuotation', 'BackendController@DeleteQuotation');


    Route::post('AddProduct', 'BackendController@AddProduct');
    Route::get('ShowProduct', 'BackendController@ShowProduct');
    Route::get('ShowProductById/{id}', 'BackendController@ShowProductById');
    Route::post('UpdateProduct', 'BackendController@UpdateProduct');
    Route::post('DeleteProduct', 'BackendController@DeleteProduct');


    Route::post('AddAgent', 'BackendController@AddAgent');
    Route::get('ShowAgent', 'BackendController@ShowAgent');
    Route::get('ShowAgentById/{id}', 'BackendController@ShowAgentById');
    Route::post('UpdateAgent', 'BackendController@UpdateAgent');
    Route::post('DeleteAgent', 'BackendController@DeleteAgent');

    Route::get('AgentShowClients', 'BackendController@AgentShowClients');
    Route::get('AgentShowAgents', 'BackendController@AgentShowAgents');
    Route::get('AgentShowProducts', 'BackendController@AgentShowProducts');


//


});

Route::prefix('manage')->group(function(){
    Route::post('CreateAccount', 'BackendController@CreateAccount');
});
