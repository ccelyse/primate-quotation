<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemQuoteDetails extends Model
{
    protected $table = "system_quote_details";
    protected $fillable = ['id','system_quotation_id','system_quotation_day_id','rack_rate','product_category_id','provider_id','provider_contract_id','quantity','unit_cost','total_price','total_rack_rate'];
}
