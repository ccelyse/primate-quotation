<script src="../js/jquery.js" type="application/javascript"></script>
<script type="application/javascript">
    var data  = sessionStorage.getItem('accessToken');
    console.log(data);
    if(sessionStorage.getItem("accessToken") === null){
        window.location.href = "/";
    }
    else{
        $(document).ready(function () {
            $.ajax({
                type: 'post',
                url: "../api/auth/me",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data);
                },
                success: function (response) {
                    console.log(response);
                    JSON.stringify(response);
                    //to string

                    if (response.user.roles[0].name == 'admin'){

                        window.location = 'SystemQuotation';

                    }else if(response.user.roles[0].name == 'hospital'){

                        window.location = 'HDashboard';

                    }else if(response.user.roles[0].name == 'surveyors'){

                        window.location = 'dashboard';

                    }else {
                        window.location.href="/";
                    }
                }, error: function (xhr, status, error) {
                    if(xhr.status == 401){
//                        alert("Kindly login to proceed");
                        window.location.href="/";
                    }else{
//                        jQuery('#login_error').show();
//                        document.getElementById("login_error").style.display = "inherit";
//                        jQuery('#login_error').append('<p>'+error+'</p>');
                        console.log(xhr.responseText);
                    }
                    // console.log(xhr.responseText);
                }
//
            });
//            setInterval(request, 50000);
        });
    }


</script>
<script src="../../backend/js/sidemenu.js" defer></script>
{{--<link href="../backend/css/bootstrap.min.css" rel="stylesheet" />--}}
<link href="../backend/css/sb-admin-3.min.css" rel="stylesheet">
<link href="../formwizard1/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="../formwizard1/css/demo.css" rel="stylesheet" />
<link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<style>
    body{
        background: #455010;
    }
    .loading{
        width: 100%;
    }
    .loading img{
        display: block;
        margin: auto;
        width: 80px;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
    }
    .loading p{
        width: 100%;
        display: block;
        margin: auto;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        font-family: Circular;
        color: #fff;
    }
</style>

<body>
<div class="loading">
    <img src="backend/img/loader.gif">
</div>
</body>
