<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProvider extends Model
{
    protected $table = "product_providers";
    protected $fillable = ['id','provider_name','product_category_id'];
}
