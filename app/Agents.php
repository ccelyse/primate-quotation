<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agents extends Model
{
    protected $table = "agents";
    protected $fillable = ['id','agent_number','agent_name','agent_email','agent_phone','agent_country',
        'agent_city','agent_state','agent_address','agent_notes'];
}
