var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {

    $(document).ready(function () {
        sendRequest();

        function sendRequest() {
            $.ajax({
                type: 'post',
                url: "../api/auth/me",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {
                    $(function(){
                        var current = location.pathname;
                        $('#sidebarmenu a').each(function(){
                            var $this = $(this);
                            // if the current path is like this link, make it active
                            if($this.attr('href').indexOf(current) !== -1){
                                $this.addClass('active');
                            }
                        })
                    });
                    $.ajax({
                        type: 'get',
                        url: "../api/auth/DashboardCount",
                        dataType: 'json',
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                        },
                        success: function (responsecount) {
                            $('.img-profile').initial();
                            $('a[href$="#"]').addClass('active');
                            jQuery('#countusers').html('0'+responsecount.countusers);
                            jQuery('#countagent').html('0'+responsecount.countagent);
                            jQuery('#pendingQuotation').html('0'+responsecount.pendingQuotation);
                            jQuery('#approvedQuotation').html('0'+responsecount.approvedQuotation);
                            jQuery('#countstandard').html('0'+responsecount.countstandard);

                        },error: function (xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url: "../api/auth/ShowUserAccount",
                        dataType: 'json',
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                        },
                        data:{email:response.user.email},
                        success: function (response) {
                            JSON.stringify(response); //to string
                            //                    console.log(response);
                            if (response.response_status == 400) {
                                $('#SystemInfo').append('');
                            } else {
                                var $i = 1;
                                $.each(response, function (key, value) {
                                    $('.form-horizontal').show();
                                    $('#id_edit').val(response[0].id);
                                    $('#name_edit').val(response[0].name);
                                    $('#email_edit').val(response[0].email);

                                    $("#SystemInfo").append("" +
                                        "<tr><td class='text-center'>" + $i++ + "</td>" +
                                        '<td>' + this.name + '</td>' +
                                        '<td>' + this.email + '</td>' +
                                        '<td>' + this.updated_at + '</td>' +
                                        '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                                        '</tr>');
                                });
                                /*
                                |--------------------------------------------
                                | Load Data Table
                                |--------------------------------------------
                                */
                                var table = $('#provincesTable').DataTable();
                            }
                        },
                        error: function (xhr, status, error) {
                            jQuery('#login_error').show();
                            document.getElementById("login_error").style.display = "inherit";
                            jQuery('#login_error').append('<p>' + error + '</p>');
                        }
                    });

                }, error: function (xhr, status, error) {
                    if(xhr.status == 401){
//                        alert("Kindly login to proceed");
                        window.location.href="/";
                    }else{
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>'+error+'</p>');
                        console.log(xhr.responseText);
                    }
                    // console.log(xhr.responseText);
                }
//
            });

        }
    });
    $(document).on('click', '#update_category', function() {
        $('#update_category').html('Updating..');
        var formData = $("#editModalForm").serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/UpdateUserAccount",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(response) {
                JSON.stringify(response); //to string
                //                    console.log(response);
                jQuery('#updating_error').show();
                document.getElementById("updating_error").style.display = "inherit";
                jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);

            },
            error: function(xhr, status, error) {
                //                    jQuery('#updating_error').show();
                //                    document.getElementById("updating_error").style.display = "inherit";
                //                    jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });

    });
}
