var data  = sessionStorage.getItem('accessToken');
    if(sessionStorage.getItem("accessToken") === null){
            window.location.href = "/";
    }
    else{
        $(document).ready(function () {
            $.ajax({
                type: 'post',
                url: "../api/auth/me",
                dataType: 'json',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                },
                success: function (response) {
                    JSON.stringify(response); //to string

                    if (response.user.roles[0].name == 'admin'){

                        window.location.href = url('admin/dashboard');

                    }else if(response.user.roles[0].name == 'hospital'){

                        window.location.href = url('admin/HDashboard');

                    }else {
                        window.location.href="/";
                    }
                }, error: function (xhr, status, error) {
                    if(xhr.status == 401){
//                        alert("Kindly login to proceed");
                        window.location.href="/";
                    }else{
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>'+error+'</p>');
                        console.log(xhr.responseText);
                    }
                    // console.log(xhr.responseText);
                }
//
            });
//            setInterval(request, 50000);
        });
}

