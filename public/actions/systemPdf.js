var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {
    $(document).ready(function() {
        var url_string = location.href;
        var urlink = new URL(url_string);
        var id = urlink.searchParams.get("id");


        $.ajax({
            type: 'post',
            url: "../api/auth/GeneratePdfClient",
            dataType: 'json',
            data:{
                id:id,
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response);
                $.each(response, function (index, value) {
                    $('#agent_name').append('' + '<h3> '+ response[0].agent_name +'</h3>');
                    $('#agent_country_city').html( response[0].agent_city + "," + response[0].country_name  );
                    $('#agent_address').html( response[0].agent_address );
                    $('#agent_phone').html( response[0].agent_phone );
                    $('#agent_email').html( response[0].agent_email );
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
            //
        });
        $.ajax({
            type: 'post',
            url: "../api/auth/GeneratePdf",
            dataType: 'json',
            data:{
                id:id,
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response);

                var trHTMLlevel = '';
                $.each(response.GeneratePdf, function (index, value) {
                    $('#loop_table_level').append('' +
                        '<tr>' +
                        '<td>'+ value.quotation_day +'</td>' +
                        '</tr>');

                    $.each(value.Records, function (index, Records) {
                        $('#loop_table_level').append('' +
                            '<tr>' +
                            '<td>&nbsp;</td>'+
                            '<td>'+ Records.provider_name +'</td>' +
                            '<td>$'+ Records.unit_cost +'</td>' +
                            '<td>'+ Records.quantity +'</td>' +
                            '<td>$'+ Records.total_price +'</td>' +
                            '<td>$'+ Records.rack_rate +'</td>' +
                            '<td>'+ Records.quantity +'</td>' +
                            '<td>$'+ Records.rack_rate * Records.quantity +' </td>' +
                            '</tr>');

                    });
                });

                // $.each(response.TotalPdf, function (index, value) {
                //     // console.log(value);
                //
                //
                // });

                $('#loop_table_total').append('' +
                    '<tr>' +
                    '<td>&nbsp;</td>'+
                    '<td>Total</td>' +
                    '<td>$'+ response.TotalPdf.unit_cost +'</td>' +
                    '<td></td>' +
                    '<td>$'+ response.TotalPdf.total_price +'</td>' +
                    '<td>$'+ response.TotalPdf.rack_rate +'</td>' +
                    '<td></td>' +
                    '<td>$'+ response.TotalPdf.total_rack +' </td>' +
                    '</tr>');

                // $('#loop_table_level').html(trHTMLlevel);
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
            //
        });
    });
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
//
        });
    });
}
