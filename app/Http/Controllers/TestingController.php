<?php

namespace App\Http\Controllers;

use App\Districts;
use App\HospitalInfo;
use App\Provinces;
use App\Role;
use App\SystemQuotationDays;
use Illuminate\Http\Request;
//use Tymon\JWTAuth\Contracts\Providers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
//use Maatwebsite\Excel\Excel;
use Excel;
use JWTAuth;

class TestingController extends Controller
{
    public function auth(){
        return view('backend.auth');
    }
    public function dashboard(){
//        $userId = Auth::id();

        return view('backend.dashboard');
    }


    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function H_Categorization(){
        return view('backend.H_Categorization');
    }
    public function HospitalAdd(){
        return view('backend.HospitalAdd');
    }
    public function Health_Center_Add(){
        return view('backend.Health_Center_Add');
    }
    public function ShowHealthCenterUI(){
        return view('backend.ShowHealthCenterUI');
    }
    public function ShowHospitalInfoUI(){
        return view('backend.ShowHospitalInfoUI');
    }
    public function ShowHospitalInfoMore(){
        return view('backend.ShowHospitalInfoMore');
    }
//    public function AccreditationSurveyors(){
//        return view('backend.AccreditationSurveyors');
//    }
    public function AccreditxSurvey(){
        return view('backend.AccreditxSurvey');
    }
    public function RiskArea(){
        return view('backend.RiskArea');
    }
    public function Standard(){
        return view('backend.Standard');
    }
    public function Level(){
        return view('backend.Level');
    }
    public function Perform_finding(){
        return view('backend.Perform_finding');
    }
    public function AccreditationSurveyors(){
        return view('backend.AccreditationSurveyors');
    }




// Hospital Account

    public function HDashboard(){
//        $userId = Auth::id();
        return view('backend.Hdashboard');
    }
    public function SystemAccount(){
        return view('backend.SystemAccount');
    }
    public function ShowHospital(){
        return view('backend.ShowHospital');
    }

//    SystemSurveyors
    public function SystemClients(){
        return view('backend.SystemClients');
    }
    public function SystemProducts(){
        return view('backend.SystemProducts');
    }
    public function SystemContracts(){
        return view('backend.SystemContracts');
    }
    public function SystemQuotation(){
        return view('backend.SystemQuotation');
    }
    public function SystemQuotationDays(){
        return view('backend.SystemQuotationDays');
    }
    public function SystemQuotationDaysServices(){
        return view('backend.SystemQuotationDaysServices');
    }
    public function SystemProductCategories(){
        return view('backend.SystemProductCategories');
    }
    public function SystemProductProvider(){
        return view('backend.SystemProductProvider');
    }
    public function SystemAgents(){
        return view('backend.SystemAgents');
    }
    public function SystemPdf(){
        return view('backend.SystemPdf');
    }
    public function SurveyHospital(){
        return view('backend.SurveyHospital');
    }
    public function ShowHospitalInfoMoreS(){
        return view('backend.ShowHospitalInfoMoreS');
    }
    public function ShowHealthCenterS(){
        return view('backend.ShowHealthCenterS');
    }
    public function CheckNewDate(){
        $check_days = SystemQuotationDays::where('quotation_id','1')->get()->last();
        $date = strtotime("+1 day", strtotime($check_days->quotation_day));
        $new_date = date("Y-m-d", $date);


    }
    public function UploadExcelHospital(){
//        $user = JWTAuth::parseToken()->authenticate();
//        dd($user);
        if( Input::file('hospital_excel') ) {
            $path = Input::file('hospital_excel')->getRealPath();
        }
        else {
            return "Error";
        }
        $data = Excel::load($path)->get();

        if($data->count()){
            foreach ($data as $value) {
                $province_id = Provinces::where('name', 'like', '%' . $value['province_id'] . '%')->first();
                $district_id = Districts::where('district_name', 'like', '%' . $value['district_id'] . '%')->first();

                if(!empty($province_id) && !empty($district_id)) {
                    HospitalInfo::create(
                        [
                            'province_id' => $province_id->id,
                            'district_id' => $district_id->id,
                            'facility_name' => $value['facility_name'],
                            'hospital_cat_id' => $value['hospital_cat_id'],
                            'facility_phone' => $value['facility_phone'],
                            'facility_email' => $value['facility_email'],
                            'name_of_facility_head' => $value['name_of_facility_head'],
                            'phone_of_head_facility' => $value['phone_of_head_facility'],
                            'name_alternative_facility_head' => $value['name_alternative_facility_head'],
                            'phone_alternative_facility_head' => $value['phone_alternative_facility_head'],
                            'population_covered' => $value['population_covered'],
                            'total_beds' => $value['total_beds'],
                            'maternity_beds' => $value['maternity_beds'],
                            'operational_ambulances' => $value['operational_ambulances'],
                            'general_practitioner' => $value['general_practitioner'],
                            'specialists' => $value['specialists'],
                            'a1' => $value['a1'],
                            'a2' => $value['a2'],
                            'mid_wives' => $value['mid_wives'],
                            'lab_techs' => $value['lab_techs'],
                            'physio' => $value['physio'],
                            'anesthetists' => $value['anesthetists'],
                            'pharmacists' => $value['pharmacists'],
                            'dentists' => $value['dentists'],
//                            'user_id' => '',
                        ]
                    );
                }
            }
        }
        return back()->with('success','You have successfully upload hospital information');
    }
    public function AssessmentPeriod(){
        return view('backend.AssessmentPeriod');
    }
    public function SurveyReports(){
        return view('backend.SurveyReports');
    }
    public function AccreditationReport(){
        return view('backend.AccreditationReport');
    }
    public function AccreditationReportPdf(){
        return view('backend.AccreditationReportPdf');
    }
    public function HospitalProfile(){
        return view('backend.HospitalProfile');
    }
}
