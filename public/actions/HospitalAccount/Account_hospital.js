var data  = sessionStorage.getItem('accessToken');
if(!data == data){
    window.location.href = "/";
}else{
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                $.ajax({
                    type: 'get',
                    url: "../api/auth/DashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                    },
                    success: function (responsecount) {
                        JSON.stringify(responsecount);
                        $('.img-profile').initial();
                        $('a[href$="#"]').addClass('active');
                        jQuery('#countusers').html('0'+responsecount.countusers);
                        jQuery('#countagent').html('0'+responsecount.countagent);
                        jQuery('#pendingQuotation').html('0'+responsecount.pendingQuotation);
                        jQuery('#approvedQuotation').html('0'+responsecount.approvedQuotation);
                        jQuery('#countstandard').html('0'+responsecount.countstandard);

                    },error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
                $(document).ready(function () {
                    $.ajax({
                        type: 'post',
                        url: "../api/auth/ShowHospitalInfoUser",
                        dataType: 'json',
                        beforeSend : function( xhr ) {
                            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                        },
                        data:{
                            id:response.user.id,
                        },
                        success: function (response) {
                            $(function(){
                                var current = location.pathname;
                                $('#sidebarmenu a').each(function(){
                                    var $this = $(this);
                                    // if the current path is like this link, make it active
                                    if($this.attr('href').indexOf(current) !== -1){
                                        $this.addClass('active');
                                    }
                                })
                            });
                            if(response.response_status == 400){
                                $('#Hospital_Info').append('');
                            }else {
                                var $i = 1;
                                $.each(response, function (key, value) {
                                    $("#Hospital_Info").append("" +
                                        "<tr><td class='text-center'>" + $i++ +"</td>" +
                                        '<td>' + this.name + '</td>' +
                                        '<td>' + this.district_name + '</td>' +
                                        '<td>' + this.facility_name + '</td>' +
                                        '<td>' + this.hospital_cat_id + '</td>' +
                                        '<td>' + this.name_of_facility_head + '</td>' +
                                        '<td>' + this.phone_of_head_facility + '</td>' +
                                        '<td><a href="/ShowHospitalInfoMore?id=' + this.id + '" class="btn btn-info btn-circle action_btn"> <i class="fas fa-eye"></i> </a></td>' +
                                        '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                                        '</tr>');
                                });
                                /*
                                |--------------------------------------------
                                | Load Data Table
                                |--------------------------------------------
                                */
                                var table = $('#provincesTable').DataTable();
                            }

                        }, error: function (xhr, status, error) {
                            jQuery('#login_error').show();
                            document.getElementById("login_error").style.display = "inherit";
                            jQuery('#login_error').append('<p>'+error+'</p>');
                        }
//
                    });
//            setInterval(request, 50000);
                });
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
        });
    });
    $(document).on('click', '.edit-modal', function() {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: '../api/auth/ShowEditHospitalInfo/' + id +'',
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {

                JSON.stringify(response); //to string
//                    console.log(response);
                $.each(response, function (index, value) {
                    $('#province_id_edit').append('' +
                        '<option value="'+ this.province_id +'" selected>'+ this.name +'</option>');
                });
                $.each(response, function (index, value) {
                    $('#district_id_edit').append('' +
                        '<option value="'+ this.district_id +'" selected>'+ this.district_name +'</option>');
                });
                $.each(response, function (index, value) {
                    $('#hospital_cat_id_edit').append('' +
                        '<option value="'+ this.hospital_cat_id +'" selected>'+ this.hospital_cat_id +'</option>');
                });
                $('.form-horizontal').show();
                $('#id_edit').val(response[0].id );
                $('#facility_name_edit').val(response[0].facility_name);
                $('#facility_phone_edit').val(response[0].facility_phone);
                $('#facility_email_edit').val(response[0].facility_email);
                $('#name_of_facility_head_edit').val(response[0].name_of_facility_head);
                $('#phone_of_head_facility_edit').val(response[0].phone_of_head_facility);
                $('#name_alternative_facility_head_edit').val(response[0].name_alternative_facility_head);
                $('#phone_alternative_facility_head_edit').val(response[0].phone_alternative_facility_head);
                $('#population_covered_edit').val(response[0].population_covered);
                $('#total_beds_edit').val(response[0].total_beds);
                $('#maternity_beds_edit').val(response[0].maternity_beds);
                $('#operational_ambulances_edit').val(response[0].operational_ambulances);
                $('#general_practitioner_edit').val(response[0].general_practitioner);
                $('#specialists_edit').val(response[0].specialists);
                $('#a1_edit').val(response[0].a1);
                $('#a2_edit').val(response[0].a2);
                $('#mid_wives_edit').val(response[0].mid_wives);
                $('#lab_techs_edit').val(response[0].lab_techs);
                $('#physio_edit').val(response[0].physio);
                $('#anesthetists_edit').val(response[0].anesthetists);
                $('#pharmacists_edit').val(response[0].pharmacists);
                $('#dentists_edit').val(response[0].dentists);

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + response.response_message + '</p>');
            }
        });
    });
    $(document).on('click', '#update_category', function () {
        $('#update_category').html('Updating..');
        var formData = $("#editModalForm").serialize();
        console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/UpdateHospitalInfo",
            data: formData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function (response) {
                JSON.stringify(response); //to string
                console.log(response);
                jQuery('#updating_error').show();
                document.getElementById("updating_error").style.display = "inherit";
                jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                setTimeout(function() { window.location.reload() }, 2000);

            }, error: function (xhr, status, error) {
//                    jQuery('#updating_error').show();
//                    document.getElementById("updating_error").style.display = "inherit";
//                    jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });

    });

}
