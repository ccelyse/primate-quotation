<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemQuotation extends Model
{
    protected $table = "system_quotations";
    protected $fillable = ['id','agent_id','quotation_name','travel_start_date','user_id'];
}
