var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {
    $(document).ready(function() {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowProductCategory",
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response); //to string
                $.each(response, function(index, value) {
                    $('.ProductsService').append('' +
                        '<option value="' + this.id + '">' + value.product_categories_name + '</option>');
                });
            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
            //
        });

        $(".note-popover").remove();
        var i=1;
        $('#add').click(function(){
            i++;
            // $('.multi-field-wrapper').append('<div class="row multi-field'+i+'" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;"> <div class=\'col-lg-6\'><div class="form-group"> <select class="form-control form-control-user" name="client_country" id="Products_inc'+i+'" required> <option value="">Products</option> </select> </div></div>  <div class=\'col-lg-6\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="agent_name" id="Price_inc'+i+'" placeholder="Price" required> </div></div> <div class=\'col-lg-12\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="agent_name" id="Quantity_inc'+i+'" placeholder="Quantity" required> </div></div><button type="button" class="remove-field btn btn-danger btn-circle" id="'+i+'" style="margin-left: 10px; margin-bottom: 10px;"><i class="fas fa-trash"></i> </button></div>');
            $('.multi-field-wrapper').append('<div class="row multi-field'+i+'" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;"> <div class=\'col-lg-6\'> <div class="form-group"> <select class="form-control form-control-user ProductsService'+i+'" name="product_category_id[]" id="ProductsService'+i+'" required> <option value="">Select Product Category</option> </select> </div></div><div class=\'col-lg-6\'> <div class="form-group"> <select class="form-control form-control-user Products_Provider'+i+'" name="provider_id[]" id="Products_Provider'+i+'" required> <option value="">Select Provider</option> </select> </div></div><div class=\'col-lg-6\'> <div class="form-group"> <select class="form-control form-control-user Products_Contract'+i+'" name="provider_contract_id[]" id="Products_Contract'+i+'" required> <option value="">Select Contract</option> </select> </div></div><div class=\'col-lg-3\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="unit_cost[]" id="unit_price'+i+'" placeholder="Unit Cost" required> </div></div><div class=\'col-lg-3\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="total_price[]" id="sub_total'+i+'" placeholder="Sub Total" required> </div></div><div class=\'col-lg-12\'> <div class="form-group"> <input type="text" class="form-control form-control-user" name="quantity[]" id="quantity'+i+'" placeholder="Quantity" required></div></div><button type="button" class="remove-field btn btn-danger btn-circle" id="'+i+'" style="margin-left: 10px; margin-bottom: 10px;"><i class="fas fa-trash"></i> </button></div>');


            $.ajax({
                type: 'get',
                url: "../api/auth/ShowProductCategory",
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                },
                success: function(response) {
                    JSON.stringify(response); //to string
                    $.each(response, function(index, value) {
                        $('.ProductsService'+i+'').append('' +
                            '<option value="' + this.id + '">' + value.product_categories_name + '</option>');
                    });
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
                //
            });

            $('.ProductsService'+i+'').on('change', function() {
                var id = $('.ProductsService'+i+'').val();
                $.ajax({
                    type: "post",
                    url: "../api/auth/ShowProviderByCategory",
                    data:{
                        id:id,
                    },
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response);
                        $('.Products_Provider'+i+'').empty();
                        $('.Products_Provider'+i+'').append("<option value=''>Select Provider</option>");
                        $.each(response, function(index, value) {
                            $('.Products_Provider'+i+'').append('' +
                                '<option value="' + this.id + '">' + value.provider_name + '</option>');
                        });

                    }, error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
            $('.Products_Provider'+i+'').on('change', function() {
                var id = $('.Products_Provider'+i+'').val();
                $.ajax({
                    type: "post",
                    url: "../api/auth/ShowContractByProvider",
                    data:{
                        id:id,
                    },
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response);
                        $('.Products_Contract'+i+'').empty();
                        $('.Products_Contract'+i+'').append("<option value=''>Select Contract</option>");
                        $.each(response, function(index, value) {
                            $('.Products_Contract'+i+'').append('' +
                                '<option value="' + this.id + '">' + value.provider_contract_name + '</option>');
                        });

                    }, error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
            $('.Products_Contract'+i+'').on('change', function() {
                var contract_id = $('.Products_Contract'+i+'').val();
                var agent_id = $('#AgentShowAgents').val();
                // console.log(agent_id);
                $.ajax({
                    type: "post",
                    url: "../api/auth/ShowContractPrice",
                    data:{
                        contract_id:contract_id,
                        agent_id:agent_id,
                    },
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response);
                        if(response.response_message){
                            $('#unit_price'+i+'').val("0");
                            $('#sub_total'+i+'').val("0");
                        }else{
                            $('#unit_price'+i+'').val(response[0].products_sto_price);
                            $('#sub_total'+i+'').val(response[0].products_sto_price);

                        }

                    }, error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
            $('#quantity'+i+'').keyup(function() {
                var Quantity_ = $('#quantity'+i+'').val();
                var price = $('#sub_total'+i+'').val();
                var unit_price = $('#unit_price'+i+'').val();
                var total = Quantity_ * price;
                var total_unit = Quantity_ * unit_price;
                if(price == "0"){
                    $('#sub_total'+i+'').val(total_unit);
                }else {
                    $('#sub_total'+i+'').val(total);
                }

            });

        });
        $(document).on('click', '.remove-field', function(){
            var button_id = $(this).attr("id");
            $('.multi-field'+button_id+'').remove();
        });
    });

    $('.ProductsService').on('change', function() {
        var id = $('.ProductsService').val();
        $.ajax({
            type: "post",
            url: "../api/auth/ShowProviderByCategory",
            data:{
                id:id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response);
                $.each(response, function (index, value) {
                    $(".Products_Provider").empty();
                    $(".Products_Provider").append("<option value=''>Select Provider</option>");
                    $(response).each(function(i){
                        $(".Products_Provider").append("<option value="+response[i].id+">"+response[i].provider_name+"</option>");
                    });
                });

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $('.Products_Provider').on('change', function() {
        var id = $('.Products_Provider').val();
        $.ajax({
            type: "post",
            url: "../api/auth/ShowContractByProvider",
            data:{
                id:id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response);
                $.each(response, function (index, value) {
                    $(".Products_Contract").empty();
                    $(".Products_Contract").append("<option value=''>Select Contract</option>");
                    $(response).each(function(i){
                        $(".Products_Contract").append("<option value="+response[i].id+">"+response[i].provider_contract_name+"</option>");
                    });
                });

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $('.Products_Contract').on('change', function() {
        var contract_id = $('.Products_Contract').val();
        var agent_id = $('#AgentShowAgents').val();
        // console.log(agent_id);
        $.ajax({
            type: "post",
            url: "../api/auth/ShowContractPrice",
            data:{
                contract_id:contract_id,
                agent_id:agent_id,
            },
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response);
                if(response.response_message){
                    $('#unit_price').val("0");
                    $('#sub_total').val("0");
                }else{
                    $('#unit_price').val(response[0].products_sto_price);
                    $('#sub_total').val(response[0].products_sto_price);

                }

            }, error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    });
    $('#quantity').keyup(function() {
        var Quantity_ = $('#quantity').val();
        var price = $('#sub_total').val();
        var unit_price = $('#unit_price').val();
        var total = Quantity_ * price;
        var total_unit = Quantity_ * unit_price;
        if(price == "0"){
            $('#sub_total').val(total_unit);
        }else {
            $('#sub_total').val(total);
        }

    });
    $.ajax({
        type: 'get',
        url: "../api/auth/AgentShowClients",
        dataType: 'json',
        beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
        },
        success: function (response) {
            JSON.stringify(response); //to string
            $(function(){
                var current = location.pathname;
                $('#sidebarmenu a').each(function(){
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if($this.attr('href').indexOf(current) !== -1){
                        $this.addClass('active');
                    }
                })
            });
            $.ajax({
                type: 'get',
                url: "../api/auth/DashboardCount",
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                },
                success: function (response) {
                    JSON.stringify(response); //to string

                    jQuery('#countusers').html('0'+response.countusers);
                    jQuery('#countagent').html('0'+response.countagent);
                    jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                    jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                    jQuery('#countstandard').html('0'+response.countstandard);

                }, error: function (xhr, status, error) {
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + error + '</p>');
                }
                //
            });
            $('.img-profile').initial();
            $.each(response, function(index, value) {
                $('#AgentShowClients').append('' +
                    '<option value=' + this.id + '>' + value.client_name + '</option>');
            });
        }, error: function (xhr, status, error) {
            if(xhr.status == 401){
                window.location.href="/";
            }else{
                console.log(xhr.responseText);
            }
        }
    });
    $.ajax({
        type: 'get',
        url: "../api/auth/AgentShowAgents",
        dataType: 'json',
        beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
        },
        success: function (response) {
            JSON.stringify(response); //to string
            $.each(response, function(index, value) {
                $('#AgentShowAgents').append('' +
                    '<option value=' + this.id + '>' + value.agent_name + '</option>');
            });
        }, error: function (xhr, status, error) {
            if(xhr.status == 401){
                window.location.href="/";
            }else{
                console.log(xhr.responseText);
            }
        }
    });
    $.ajax({
        type: 'post',
        url: "../api/auth/me",
        dataType: 'json',
        beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
        },
        success: function (response) {
            JSON.stringify(response); //to string
            $('#user_id').val(response.user.id);
            $(function(){
                var current = location.pathname;
                $('#sidebarmenu a').each(function(){
                    var $this = $(this);
                    // if the current path is like this link, make it active
                    if($this.attr('href').indexOf(current) !== -1){
                        $this.addClass('active');
                    }
                })
            });
            $.ajax({
                type: 'get',
                url: "../api/auth/DashboardCount",
                dataType: 'json',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                },
                success: function (response) {
                    JSON.stringify(response); //to string

                    jQuery('#countusers').html('0'+response.countusers);
                    jQuery('#countagent').html('0'+response.countagent);
                    jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                    jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                    jQuery('#countstandard').html('0'+response.countstandard);

                }, error: function (xhr, status, error) {
                    jQuery('#login_error').show();
                    document.getElementById("login_error").style.display = "inherit";
                    jQuery('#login_error').append('<p>' + error + '</p>');
                }
                //
            });
            $('.img-profile').initial();
        }, error: function (xhr, status, error) {
            if(xhr.status == 401){
                window.location.href="/";
            }else{
                console.log(xhr.responseText);
            }
        }
//
    });
    $(document).on('click', '#addquotation', function() {
        $('#addquotation').html('Adding....');
        var formData = $("#AddQuote").serialize();
        // console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/AddQuotation",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(data, xhr, response) {
                JSON.stringify(response); //to string
                jQuery('#added_error').show();
                document.getElementById("added_error").style.display = "inherit";
                jQuery('#added_error').append('<p>' + data.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);
            },
            error: function(xhr, status, error) {
                // jQuery('#login_error').show();
                // document.getElementById("login_error").style.display = "inherit";
                // jQuery('#login_error').append('<p> ' + data.message + '</p>');
                console.log(xhr.responseText);
            }

        });

    });
    $.ajax({
        type: 'get',
        url: "../api/auth/ShowQuotation",
        dataType: 'json',
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'BEARER ' + data);
        },
        success: function(response) {
            if (response.response_status == 400) {
                $('#Categorization_info').append('');
            } else {

                var $i = 1;
                $.each(response, function (key, value) {
                    $("#Accreditation_info").append("" +
                        "<tr><td class='text-center'>" + $i++ +"</td>" +
                        '<td>' + this.created_at + '</td>' +
                        '<td><a href="/SystemQuotationDays?id=' + this.id + '" class="action_btn" id="' + this.id + '">' + this.agent_name + '</a></td>' +
                        '<td>' + this.quotation_name + '</td>' +
                        '<td>' + this.travel_start_date + '</td>' +
                        '<td><a href="/SystemPdf?id=' + this.id + '" class="btn btn-success action_btn" id="' + this.id + '">Quotation</a></td>' +
                        '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                        '</tr>');
                });
                /*
                |--------------------------------------------
                | Load Data Table
                |--------------------------------------------
                */
                var table = $('#provincesTable').DataTable();
            }

        },
        error: function(xhr, status, error) {
            console.log(xhr.responseText);
        }
        //
    });
    $(document).on('click', '.delete', function() {
        var id = $(this).attr("id");
           // alert(id);
        if (confirm("Are you sure you want to delete this records?")) {
            $.ajax({
                type: 'post',
                url: "../api/auth/DeleteQuotation",
                dataType: 'json',
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                },
                data: {
                    id: id
                },
                success: function(response) {
                    JSON.stringify(response); //to string
                    jQuery('#delete_info').show();
                    document.getElementById("delete_info").style.display = "block";
                    jQuery('#delete_info').append('<p>' + response.response_message + '</p>');
                    setTimeout(function() {
                        window.location.reload()
                    }, 2000);

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }
    });
}
