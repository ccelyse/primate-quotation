<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderContracts extends Model
{
    protected $table = "provider_contracts";
    protected $fillable = ['id','provider_id','provider_contract_name','product_category_id'];
}
