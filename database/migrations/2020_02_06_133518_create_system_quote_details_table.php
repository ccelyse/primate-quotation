<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemQuoteDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_quote_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('system_quotation_id');
            $table->string('system_quotation_day_id');
            $table->string('product_category_id');
            $table->string('provider_id');
            $table->string('provider_contract_id');
            $table->string('quantity');
            $table->string('rack_rate');
            $table->string('unit_cost');
            $table->string('total_price');
            $table->string('total_rack_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_quote_details');
    }
}
