<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsSTOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_s_t_os', function (Blueprint $table) {
            $table->increments('id');
            $table->string('products_id');
            $table->string('products_agent_id');
            $table->string('provider_contract_id');
            $table->string('products_sto_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_s_t_os');
    }
}
