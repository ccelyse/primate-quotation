@extends('backend.layout.master')
@section('title', 'Accreditx')
@section('content')

<script src="../../backend/js/sidemenu.js" defer></script>
<link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
<link href="../../formwizard/css/demo.css" rel="stylesheet" />
<link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<!--   Big container   -->

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebarmenu">

    </div>
    <div id='content-wrapper' class='d-flex flex-column'>
        <div id='content'>
            <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

            </nav>
            <div class='container-fluid'>

                <div class="row">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">System User</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="provincesTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Created At</th>
                                        <th>Edit</th>
                                        {{--<th>Delete</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody id="SystemInfo">

                                    </tbody>
                                </table>
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Account</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="#" id="editModalForm">
                                                    {{--<div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>--}}
                                                    <div class="tab-content">
                                                        <div class="row">
                                                            <div class='col-lg-12'>
                                                                <div class="form-group" hidden>
                                                                    <input type="text" class="form-control form-control-user" name="id" id="id_edit" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="name" id="name_edit" placeholder="Name">
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="email" id="email_edit" placeholder="Email">
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                                <button type='button' class='btn btn-fill btn-success btn-wd' value='Update' id="update_category" style="border-radius:0px !important;">Update changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        <!-- end row -->

    </div>
    <!--  big container -->

</div>

@include('backend.layout.footer')
</div>
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


<!--   Core JS Files   -->
<script src="../js/jquery.js" type="application/javascript"></script>

<script src="../../formwizard/js/jquery-2.2.4.min.js" type="text/javascript"></script>
{{--
<script src="../formwizard/js/bootstrap.min.js" type="text/javascript"></script>--}}
<script src="../../formwizard/js/jquery.bootstrap.js" type="text/javascript"></script>
<script src="../../formwizard/js/material-bootstrap-wizard.js"></script>
<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="../../formwizard/js/jquery.validate.min.js"></script>

<script src="../../backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../../backend/js/custom.js"></script>

<script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Page level custom scripts -->
<script src="../../js/demo/datatables-demo.js"></script>
<script src="../../js/initial.min.js"></script>
<script src="../../actions/surveyor/system_account.js"></script>
@endsection
