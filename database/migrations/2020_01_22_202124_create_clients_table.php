<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_number');
            $table->string('client_name');
            $table->string('client_email');
            $table->string('client_phone');
            $table->string('client_website')->nullable();
            $table->string('client_country');
            $table->string('client_city');
            $table->string('client_state')->nullable();
            $table->string('client_address');
            $table->string('client_notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
